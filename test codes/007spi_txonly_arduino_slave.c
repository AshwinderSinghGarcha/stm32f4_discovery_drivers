/*
 * 006spi_tx_testing.c
 *
 *  Created on: Dec 8, 2020
 *      Author: Developer
 */
#include <string.h>
#include <stdint.h>
#include <stdio.h>

#include "stm32f4xx.h"
//#include "stm32f4xx_spi_driver.h"
// SPI 2 MODE

//PORT B            ALT MODE
//PB15-->  MOSI       AF5
//PB14-->  MISO       AF5
//PB13-->  SCLK       AF5
//PB12-->  NSS        AF5

void SPI2_GPIOInits()
{
	GPIO_Handle_t SPIPins;
	SPIPins.pGPIOx = GPIOB;

	SPIPins.GPIO_PinConfiguration.GPIO_PinMode = GPIO_MODE_ALT;
	SPIPins.GPIO_PinConfiguration.GPIO_PinALTFunMode = GPIO_AF5;
	SPIPins.GPIO_PinConfiguration.GPIO_PinOPTYPE = GPIO_TPYE_PP;
	SPIPins.GPIO_PinConfiguration.GPIO_PinPUPDControl = GPIO_PUPD_PU;
	SPIPins.GPIO_PinConfiguration.GPIO_PinSpeed = GPIO_SPEED_HI;
	// sclk
	SPIPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_13;
	GPIO_Init(&SPIPins);

	// MOSI
	SPIPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_15;
	GPIO_Init(&SPIPins);
	GPIO_PeriClkControl(GPIOB, ENABLE);           // clk enable
	SPI_PeriClockControl(SPI2, ENABLE);           //clk enable

	// MISO
	//SPIPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_14;
	//GPIO_Init(&SPIPins);



	// NSS
	SPIPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_12;
	GPIO_Init(&SPIPins);

}

void SPI2_Inits()
{
	SPI_Handle_t SPI2handle;
	SPI2handle.pSPIx = SPI2;
	SPI2handle.SPIConfig.SPI_BusConfig = SPI_FULL_Dup   ;
	SPI2handle.SPIConfig.SPI_DevideMode= SPI_MASTER     ;
	SPI2handle.SPIConfig.SPI_SclkSpeed = SPI_SPEED_DIV_8;     //generetae clock of 2mhz
	SPI2handle.SPIConfig.SPI_DFF       = SPI_DFF_8BIT   ;
	SPI2handle.SPIConfig.SPI_CPOL      = SPI_CPOL_LOW   ;
	SPI2handle.SPIConfig.SPI_CPHA      = SPI_CPHA_LOW   ;
	SPI2handle.SPIConfig.SPI_SSM       = SPI_SSM_DI     ;      //Arduino as slave sp use nss

	SPI_Init(&SPI2handle);
}

void GPIO_buttonInit()
{
	GPIO_Handle_t BUTTON;
    BUTTON.pGPIOx =GPIOB;
    BUTTON.GPIO_PinConfiguration.GPIO_PinNumber=GPIO_PIN_NO_0;
    BUTTON.GPIO_PinConfiguration.GPIO_PinMode = GPIO_MODE_IN;
    BUTTON.GPIO_PinConfiguration.GPIO_PinSpeed = GPIO_SPEED_HI;
    BUTTON.GPIO_PinConfiguration.GPIO_PinPUPDControl=GPIO_PUPD_PD;
    GPIO_PeriClkControl(GPIOB, ENABLE);
    GPIO_Init(&BUTTON);
}
void delay(void)
{
	for (uint32_t i=0;i<500000;i++);
}
int main()
{
	GPIO_buttonInit();

	char  user_data[] = "hello world";
	// init gpio pins working as SPI2
	SPI2_GPIOInits();
    // init SPI2 parameters
	SPI2_Inits();
     // we use NSS pin no need to puoll high internally by SSI

	//enable SSI =1 forpulling high NSS signal internally to

	//SPI_SSIConfig(SPI2, ENABLE);

	SPI_SSOEConfig(SPI2, ENABLE);
	while (1)
	{
		while (!(GPIO_ReadFromInputPin(GPIOB,GPIO_PIN_NO_0)));
		delay(); //debouncing

		//enable SPI2
		SPI_PeripheralControl(SPI2, ENABLE);      // enable bit 6

		// first send length of data to arduino
		uint8_t dataLen = strlen(user_data);

		SPI_SendData(SPI2,&dataLen,1);
		//second send data
		SPI_SendData(SPI2,(uint8_t*)user_data,strlen(user_data));    // so hello world send over miso


		//wait for last data send before closing SPI comm by using busy flag
		while (BUSY_flag(SPI2));

		//DISABLE  SPI2
		SPI_PeripheralControl(SPI2, DISABLE);
	}

	return 0;
}



