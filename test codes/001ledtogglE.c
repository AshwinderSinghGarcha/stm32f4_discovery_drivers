/*
 * 001ledtogglE.c
 *
 *  Created on: Dec 3, 2020
 *      Author: Developer
 */

#include "stm32f4xx.h"

void delay(void)
{
	for (uint32_t i=0;i<500000;i++);
}
int main(void)
{
	GPIO_Handle_t GpioLed;
	GpioLed.pGPIOx = GPIOD;
	GpioLed.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_12;
	GpioLed.GPIO_PinConfiguration.GPIO_PinMode = GPIO_MODE_OUT;
	GpioLed.GPIO_PinConfiguration.GPIO_PinSpeed= GPIO_SPEED_HI;
	GpioLed.GPIO_PinConfiguration.GPIO_PinOPTYPE =GPIO_TPYE_PP;
    GpioLed.GPIO_PinConfiguration.GPIO_PinPUPDControl = GPIO_PUPD_NO;

    GPIO_PeriClkControl(GPIOD, ENABLE);
    GPIO_Init(&GpioLed);
    while(1)
    {
    GPIO_ToggleOutputPin(GPIOD,GPIO_PIN_NO_12);
    delay();
    }

}

