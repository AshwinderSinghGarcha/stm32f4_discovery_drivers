/*
 * 012_uart_tx.c
 *
 *  Created on: Dec 23, 2020
 *      Author: Developer
 */


#include <string.h>
#include <stdint.h>
#include <stdio.h>

#include "stm32f4xx.h"
//#include "stm32f4xx_spi_driver.h"
// SPI 2 MODE

//PORT A            ALT MODE
//PA2-->  tx       AF7
//PA3-->  rx       AF7
char msg[1024] = "UART Tx testing...\n";
USART_Handle_t usart2_handle;
void USART_GPIOInits()
{
	GPIO_PeriClkControl(GPIOA, ENABLE);
	GPIO_Handle_t UAPins;
	UAPins.pGPIOx = GPIOA;

	UAPins.GPIO_PinConfiguration.GPIO_PinMode = GPIO_MODE_ALT;
	UAPins.GPIO_PinConfiguration.GPIO_PinALTFunMode = GPIO_AF7;
	UAPins.GPIO_PinConfiguration.GPIO_PinOPTYPE = GPIO_TPYE_PP;
	UAPins.GPIO_PinConfiguration.GPIO_PinPUPDControl = GPIO_PUPD_PU;
	UAPins.GPIO_PinConfiguration.GPIO_PinSpeed = GPIO_SPEED_HI;
	// rX
	UAPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_3;
	GPIO_Init(&UAPins);

	// TX
	UAPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_2;
	GPIO_Init(&UAPins);


}

void usart2_Inits()
{
	USART_PeriClockControl(USART2 , ENABLE);
	usart2_handle.pUSARTx = USART2;

	usart2_handle.USART_Config.USART_Baud = USART_STD_BAUD_9600 ;
	usart2_handle.USART_Config.USART_HWFlowControl=USART_HW_FLOW_CTRL_NONE;
	usart2_handle.USART_Config.USART_Mode = USART_MODE_ONLY_TX;
	usart2_handle.USART_Config.USART_NoOfStopBits =USART_STOPBITS_1;
	usart2_handle.USART_Config.USART_WordLength = USART_WORDLEN_8BITS;
	usart2_handle.USART_Config.USART_ParityControl = USART_PARITY_DISABLE;
	USART_Init(&usart2_handle);



}

void delay(void)
{
	for (uint32_t i=0;i<500000;i++);
}
void GPIO_ButtonInit(void)
{
    GPIO_PeriClkControl(GPIOB, ENABLE);

    GPIO_Handle_t BUTTON;


    BUTTON.pGPIOx =GPIOB;
    BUTTON.GPIO_PinConfiguration.GPIO_PinNumber=GPIO_PIN_NO_0;
    BUTTON.GPIO_PinConfiguration.GPIO_PinMode = GPIO_MODE_IN;
    BUTTON.GPIO_PinConfiguration.GPIO_PinSpeed = GPIO_SPEED_HI;
    BUTTON.GPIO_PinConfiguration.GPIO_PinPUPDControl=GPIO_PUPD_PD;
    GPIO_Init(&BUTTON);
}
int main(void)
{

	GPIO_ButtonInit();

	USART_GPIOInits();


	usart2_Inits();



	USART_PeripheralControl(USART2,ENABLE);


	while(1)
	{
		 while(! GPIO_ReadFromInputPin(GPIOB,GPIO_PIN_NO_0));

		 delay(); //debouncing


		 USART_SendData(&usart2_handle,(uint8_t*)msg,strlen(msg));
	}
	return 0;
}



