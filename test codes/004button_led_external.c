/*
 * 001ledtogglE.c
 *
 *  Created on: Dec 3, 2020
 *      Author: Developer
 */

#include "stm32f4xx.h"

void delay(void)
{
	for (uint32_t i=0;i<500000;i++);
}
int main(void)
{   uint8_t x;
    // output led PA8
    GPIO_Handle_t LED,BUTTON;
    LED.pGPIOx =GPIOA;
    LED.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_8;
    LED.GPIO_PinConfiguration.GPIO_PinMode =GPIO_MODE_OUT;
    LED.GPIO_PinConfiguration.GPIO_PinSpeed = GPIO_SPEED_HI;
    LED.GPIO_PinConfiguration.GPIO_PinOPTYPE =GPIO_TPYE_PP;
    LED.GPIO_PinConfiguration.GPIO_PinPUPDControl=GPIO_PUPD_NO;

    BUTTON.pGPIOx =GPIOB;
    BUTTON.GPIO_PinConfiguration.GPIO_PinNumber=GPIO_PIN_NO_0;
    BUTTON.GPIO_PinConfiguration.GPIO_PinMode = GPIO_MODE_IN;
    BUTTON.GPIO_PinConfiguration.GPIO_PinSpeed = GPIO_SPEED_HI;
    BUTTON.GPIO_PinConfiguration.GPIO_PinPUPDControl=GPIO_PUPD_PD;
    GPIO_PeriClkControl(GPIOB, ENABLE);
    GPIO_PeriClkControl(GPIOA, ENABLE);
    GPIO_Init(&LED);
    GPIO_Init(&BUTTON);


    while(1){
    x= GPIO_ReadFromInputPin(GPIOB,GPIO_PIN_NO_0);
    if (x==1)
    {
    	delay(); //debouncing
    	GPIO_ToggleOutputPin(GPIOA,GPIO_PIN_NO_8);

    }
    }
}
// PUSHPULL MODE LED ON PA0 CONNECT TO GND  you dont need PU PD resistor
// EITHER USE PD INTERNAL OF EXTERNAL FOR SWITCH CONNECT TO 5V
