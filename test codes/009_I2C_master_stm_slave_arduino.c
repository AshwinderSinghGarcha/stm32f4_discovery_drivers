

#include<stdint.h>
#include<stdio.h>
#include"stm32f4xx.h"
#include <string.h>
/**
 * I2C alt. fnc. -> 4
 * PB6 -> SCL
 * PB7 -> SDA
 */

I2C_Handle_t I2C;

//Some data to be sent to slave
//Arduino sketch uses Arduino Wire library which cannot send/receive
//more than 32 bytes in a single I2C transaction
uint8_t myData[] = "I2C transmission test\n";

#define SLAVE_ADDR 0x68 //Address of Arduino

void I2C1_GPIOInit(void){ //configure I2C pins
	GPIO_Handle_t I2CPins;

	I2CPins.pGPIOx = GPIOB;
	I2CPins.GPIO_PinConfiguration.GPIO_PinMode =  GPIO_MODE_ALT;
	I2CPins.GPIO_PinConfiguration.GPIO_PinOPTYPE =GPIO_TPYE_OD;
	I2CPins.GPIO_PinConfiguration.GPIO_PinPUPDControl = GPIO_PUPD_PU;; /*Normally we need to calculate a proper pull-up resistor value (should be 966ohm<Rp<7.86kohm) according
	to I2C specifications but in this application internal pull-up resistors are sufficient (even though they are so high) since jumper cables used are so short and
	communication speed is not high*/
	I2CPins.GPIO_PinConfiguration.GPIO_PinALTFunMode = 4; //I2C mode
	I2CPins.GPIO_PinConfiguration.GPIO_PinSpeed = GPIO_SPEED_HI;

	I2CPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_6; //SCL PB6
	GPIO_Init(&I2CPins);

	I2CPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_7; //SDA PB7
	GPIO_Init(&I2CPins);
}

void I2C1_Init (void){ //Configure I2C1 interface

	I2C.pI2Cx = I2C1;
	I2C.I2CConfig.I2C_ACKControl = ENABLE;
	I2C.I2CConfig.I2C_DeviceAddress=0x61; //This value does not matter in master transmission
	I2C.I2CConfig.I2C_FMDutyCycle=I2C_DUTY_2; //This value does not matter since we are in standard mode
	I2C.I2CConfig.I2C_SclkSpeed=I2C_SCL_SPEED_SM; //100kHZ
	I2C_Init(&I2C);
}


void PB_INIT()
{
	GPIO_PeriClkControl(GPIOB, ENABLE);

	//PB0 pb

	GPIO_Handle_t pb;
	pb.pGPIOx = GPIOB;
	pb.GPIO_PinConfiguration.GPIO_PinMode = GPIO_MODE_IN;
	pb.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_0;

	pb.GPIO_PinConfiguration.GPIO_PinPUPDControl= GPIO_PUPD_PD;
	pb.GPIO_PinConfiguration.GPIO_PinSpeed=GPIO_SPEED_VHI;

	GPIO_Init(&pb);
}
int main()
{
	//Initialize button pin
	PB_INIT();

	//Initialize GPIO pins
	I2C1_GPIOInit();

	//Initialize I2C peripheral
	I2C1_Init();

	//Enable I2C peripheral
	I2C_PeripheralControl(I2C1, ENABLE);

	while(1)
	{
	//Wait for button press
	while (!(GPIO_ReadFromInputPin(GPIOB, GPIO_PIN_NO_0))){};
	for(int i = 0; i < 300000; i++);

	//Send some data to the slave

	I2C_MasterSendData(&I2C, myData,strlen((char *)myData), SLAVE_ADDR);

	}
}
