/*
 * 001ledtogglE.c
 *
 *  Created on: Dec 3, 2020
 *      Author: Developer
 */

#include "stm32f4xx.h"

void delay(void)
{
	for (uint32_t i=0;i<500000;i++);
}
int main(void)
{   uint8_t x;

	GPIO_Handle_t GpioLed;
	GpioLed.pGPIOx = GPIOD;
	GpioLed.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_12;
	GpioLed.GPIO_PinConfiguration.GPIO_PinMode = GPIO_MODE_OUT;
	GpioLed.GPIO_PinConfiguration.GPIO_PinSpeed= GPIO_SPEED_HI;
	GpioLed.GPIO_PinConfiguration.GPIO_PinOPTYPE =GPIO_TPYE_OD;
    GpioLed.GPIO_PinConfiguration.GPIO_PinPUPDControl = GPIO_PUPD_NO;

    GPIO_Handle_t GpioPB;
    GpioPB.pGPIOx = GPIOA;
    GpioPB.GPIO_PinConfiguration.GPIO_PinNumber =GPIO_PIN_NO_0;
    GpioPB.GPIO_PinConfiguration.GPIO_PinMode   =GPIO_MODE_IN;
    GpioPB.GPIO_PinConfiguration.GPIO_PinSpeed= GPIO_SPEED_HI;
    GpioPB.GPIO_PinConfiguration.GPIO_PinPUPDControl = GPIO_PUPD_NO;

    GPIO_PeriClkControl(GPIOD, ENABLE);
    GPIO_PeriClkControl(GPIOA, ENABLE);
    GPIO_Init(&GpioPB);
    GPIO_Init(&GpioLed);


    while(1){
    x= GPIO_ReadFromInputPin(GPIOA,GPIO_PIN_NO_0);
    if (x==1)
    {
    	delay(); //debouncing
    	GPIO_ToggleOutputPin(GPIOD,GPIO_PIN_NO_12);

    }
    }
}
// open drain you need pull external resistor with  no internal becz its value is 40k pullup
// only use open drain is in I2C MODE
