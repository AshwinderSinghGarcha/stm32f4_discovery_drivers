
#include <string.h>
#include <stdint.h>
#include <stdio.h>

#include "stm32f4xx.h"
//#include "stm32f4xx_spi_driver.h"
// SPI 2 MODE

//PORT B            ALT MODE
//PB15-->  MOSI       AF5
//PB14-->  MISO       AF5
//PB13-->  SCLK       AF5
//PB12-->  NSS        AF5

//command codes SEND TO ARDUIO OVER MOSI PIN
#define COMMAND_LED_CTRL       0x50
#define COMMAND_SENSOR_READ    0x51
#define COMMAND_LED_READ       0x52
#define COMMAND_PRINT          0x53
#define COMMAND_ID_READ        0x54

#define LED_ON                 1
#define LED_OFF                0
//ANALOG ARDUINO PINS
#define ANALOG_PIN0            0
#define ANALOG_PIN1            1
#define ANALOG_PIN2            2
#define ANALOG_PIN3            3
#define ANALOG_PIN4            4
// ARDUINO LED
#define LED_PIN               9

void SPI2_GPIOInits()
{
	GPIO_Handle_t SPIPins;
	SPIPins.pGPIOx = GPIOB;

	SPIPins.GPIO_PinConfiguration.GPIO_PinMode = GPIO_MODE_ALT;
	SPIPins.GPIO_PinConfiguration.GPIO_PinALTFunMode = GPIO_AF5;
	SPIPins.GPIO_PinConfiguration.GPIO_PinOPTYPE = GPIO_TPYE_PP;
	SPIPins.GPIO_PinConfiguration.GPIO_PinPUPDControl = GPIO_PUPD_PU;
	SPIPins.GPIO_PinConfiguration.GPIO_PinSpeed = GPIO_SPEED_HI;
	// sclk
	SPIPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_13;
	GPIO_Init(&SPIPins);

	// MOSI
	SPIPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_15;
	GPIO_Init(&SPIPins);
	GPIO_PeriClkControl(GPIOB, ENABLE);           // clk enable
	SPI_PeriClockControl(SPI2, ENABLE);           //clk enable

	 //MISO
	SPIPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_14;
	GPIO_Init(&SPIPins);



	// NSS
	SPIPins.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_12;
	GPIO_Init(&SPIPins);

}

void SPI2_Inits()
{
	SPI_Handle_t SPI2handle;
	SPI2handle.pSPIx = SPI2;
	SPI2handle.SPIConfig.SPI_BusConfig = SPI_FULL_Dup   ;
	SPI2handle.SPIConfig.SPI_DevideMode= SPI_MASTER     ;
	SPI2handle.SPIConfig.SPI_SclkSpeed = SPI_SPEED_DIV_8;     //generetae clock of 2mhz
	SPI2handle.SPIConfig.SPI_DFF       = SPI_DFF_8BIT   ;
	SPI2handle.SPIConfig.SPI_CPOL      = SPI_CPOL_LOW   ;
	SPI2handle.SPIConfig.SPI_CPHA      = SPI_CPHA_LOW   ;
	SPI2handle.SPIConfig.SPI_SSM       = SPI_SSM_DI     ;      //Arduino as slave sp use nss

	SPI_Init(&SPI2handle);
}

void GPIO_buttonInit()
{
	GPIO_Handle_t BUTTON;
    BUTTON.pGPIOx =GPIOB;
    BUTTON.GPIO_PinConfiguration.GPIO_PinNumber=GPIO_PIN_NO_0;
    BUTTON.GPIO_PinConfiguration.GPIO_PinMode = GPIO_MODE_IN;
    BUTTON.GPIO_PinConfiguration.GPIO_PinSpeed = GPIO_SPEED_HI;
    BUTTON.GPIO_PinConfiguration.GPIO_PinPUPDControl=GPIO_PUPD_PD;
    GPIO_PeriClkControl(GPIOB, ENABLE);
    GPIO_Init(&BUTTON);
}
void delay(void)
{
	for (uint32_t i=0;i<500000;i++);
}

extern void initialise_monitor_handles(void);

uint8_t SPI_VerifyResponse(uint8_t ackbyte)
{
	if(ackbyte == 0xF5)
	{
		return 1;
	}
	return 0;
}
int main()
{
	initialise_monitor_handles();
	uint8_t dummy_write = 0xff;
	uint8_t dummy_read;
	printf("Application is running\n");
	GPIO_buttonInit();
	// init gpio pins working as SPI2
	SPI2_GPIOInits();
    // init SPI2 parameters
	SPI2_Inits();
     // we use NSS pin no need to puoll high internally by SSI
     //enable SSI =1 forpulling high NSS signal internally to

	printf("SPI Init. done\n");
	printf("STM AS MASTER Init. done\n");
	//SPI_SSIConfig(SPI2, ENABLE);
     SPI_SSOEConfig(SPI2, ENABLE);
	while (1)
	{   //when button pressed
		while (!(GPIO_ReadFromInputPin(GPIOB,GPIO_PIN_NO_0)));
		delay(); //debouncing

		//enable SPI2
		SPI_PeripheralControl(SPI2, ENABLE);      // enable bit 6

		//1. CMD_LED_CTRL <pin no>   <value>

		uint8_t commandcode = COMMAND_LED_CTRL;
		uint8_t ackbyte;
		uint8_t args[2];
		SPI_SendData(SPI2, &commandcode, 1);
		//do dummy read to clear off RXNE to make input register buffer empty rxne flag 0
		SPI_ReceiveData(SPI2, &dummy_read, 1);
		//send some dummy data to fetch response from
		SPI_SendData(SPI2, &dummy_write, 1);
		//printf("SPI_SendDataSPI2dummywrite: %x\n",dummy_write);
		SPI_ReceiveData(SPI2, &ackbyte, 1);
		//printf("SPI_SendDataSPI2ackbyte: %x\n",ackbyte);
		if (SPI_VerifyResponse(ackbyte))
		{
			// send srguments
			args[0] = LED_PIN;
			args[1] = LED_ON ;
			SPI_SendData(SPI2, args, 2);
 		}
		printf("\nCOMMAND_LED_CTRL Executed\n");


		//2. CMD_LED_CTRL COMMAND_SENSOR_READ    <value>
	    //when button pressed
		while (!(GPIO_ReadFromInputPin(GPIOB,GPIO_PIN_NO_0)));
		delay(); //debouncing

		commandcode = COMMAND_SENSOR_READ;
        SPI_SendData(SPI2, &commandcode, 1);
 		SPI_ReceiveData(SPI2, &dummy_read, 1);
 		//send some dummy data to fetch response from
 		SPI_SendData(SPI2, &dummy_write, 1);
 		SPI_ReceiveData(SPI2, &ackbyte, 1);

 		if (SPI_VerifyResponse(ackbyte))
 		{
 			// send srguments
 			args[0] = ANALOG_PIN0;
 			SPI_SendData(SPI2, args, 1);  //sending pin number

  		}
 		//do dummy read to clear off RXNE to make input register buffer empty rxne flag 0
 		SPI_ReceiveData(SPI2, &dummy_read, 1);

 		//delay
 		delay();
 		//send some dummy data to fetch response from
 		 SPI_SendData(SPI2, &dummy_write, 1);
 		printf("\nCOMMAND_SENSOR_READ Executed\n");

 		//read sensor value
 		 uint8_t Sensor_read;
 		 SPI_ReceiveData(SPI2, &Sensor_read, 1);
 		printf("SensorReading: %d\n",Sensor_read);




 		//3. command
//------------------------------------------------------------------------------------------------------------------------


 				//wait till button is pressed
		while (!(GPIO_ReadFromInputPin(GPIOB,GPIO_PIN_NO_0)));
		delay(); //debouncing

 				commandcode = COMMAND_LED_READ;

 				//send command
 				SPI_SendData(SPI2,&commandcode,1);

 				//do dummy read to clear off the RXNE
 				SPI_ReceiveData(SPI2,&dummy_read,1);

 				//Send some dummy byte to fetch the response from the slave
 				SPI_SendData(SPI2,&dummy_write,1);

 				//read the ack byte received
 				SPI_ReceiveData(SPI2,&ackbyte,1);

 				if( SPI_VerifyResponse(ackbyte))
 				{
 					args[0] = LED_PIN;

 					//send arguments
 					SPI_SendData(SPI2,args,1); //sending one byte of

 					//do dummy read to clear off the RXNE
 					SPI_ReceiveData(SPI2,&dummy_read,1);

 					//insert some delay so that slave can ready with the data
 					delay();

 					//Send some dummy bits (1 byte) fetch the response from the slave
 					SPI_SendData(SPI2,&dummy_write,1);
 					printf("\nCOMMAND_READ_LED Executed\n");
 					uint8_t led_status;
 					SPI_ReceiveData(SPI2,&led_status,1);
 					printf("LedStatus %d\n",led_status);

 				}

//-----------------------------------------------------------------------------------------------------------------

		//wait till button is pressed
 		while (!(GPIO_ReadFromInputPin(GPIOB,GPIO_PIN_NO_0)));
 		delay(); //debouncing
		commandcode = COMMAND_PRINT;

		//send command
		SPI_SendData(SPI2,&commandcode,1);

		//do dummy read to clear off the RXNE
		SPI_ReceiveData(SPI2,&dummy_read,1);

		//Send some dummy byte to fetch the response from the slave
		SPI_SendData(SPI2,&dummy_write,1);

		//read the ack byte received
		SPI_ReceiveData(SPI2,&ackbyte,1);

		uint8_t message[] = "Hello ! How are you ??";
		if( SPI_VerifyResponse(ackbyte))
		{
			args[0] = strlen((char*)message);

			//send arguments
			SPI_SendData(SPI2,args,1); //sending length

			//send message
			SPI_SendData(SPI2,message,args[0]);

			printf("\nCOMMAND_PRINT Executed \n");

		}

 		 //5. id recieve from slave

//-----------------------------------------------------
		//5. CMD_ID_READ
		//wait till button is pressed
		while (!(GPIO_ReadFromInputPin(GPIOB,GPIO_PIN_NO_0)));
 		delay(); //debouncing
		commandcode = COMMAND_ID_READ;

		//send command
		SPI_SendData(SPI2,&commandcode,1);

		//do dummy read to clear off the RXNE
		SPI_ReceiveData(SPI2,&dummy_read,1);

		//Send some dummy byte to fetch the response from the slave
		SPI_SendData(SPI2,&dummy_write,1);

		//read the ack byte received
		SPI_ReceiveData(SPI2,&ackbyte,1);

		uint8_t id[11];
		uint32_t i=0;
		if( SPI_VerifyResponse(ackbyte))
		{
			//read 10 bytes id from the slave
			for(  i = 0 ; i < 10 ; i++)
			{
				//send dummy byte to fetch data from slave
				SPI_SendData(SPI2,&dummy_write,1);
				SPI_ReceiveData(SPI2,&id[i],1);
			}

			id[11] = '\0';
			printf("\nCOMMAND_ID\n");
			printf("ID : %s \n",id);

		}





		//wait for last data send before closing SPI comm by using busy flag
		while (BUSY_flag(SPI2));

		//DISABLE  SPI2
		SPI_PeripheralControl(SPI2, DISABLE);
	}

	return 0;
}



