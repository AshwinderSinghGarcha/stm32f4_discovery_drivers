#include "stm32f4xx.h"
#include<string.h>

void delay(void)
{
	// this will introducing ~200ms delay when clock is 16mhz
	for (uint32_t i=0;i<500000/2;i++);
}
int main(void)
{   //uint8_t x;
    // output led PA8
    GPIO_Handle_t LED,BUTTON;
    memset(&LED,0,sizeof(LED));            // clearing struct before initialising
    memset(&BUTTON,0,sizeof(BUTTON));

    LED.pGPIOx =GPIOA;
    LED.GPIO_PinConfiguration.GPIO_PinNumber = GPIO_PIN_NO_8;
    LED.GPIO_PinConfiguration.GPIO_PinMode =GPIO_MODE_OUT;
    LED.GPIO_PinConfiguration.GPIO_PinSpeed = GPIO_SPEED_HI;
    LED.GPIO_PinConfiguration.GPIO_PinOPTYPE =GPIO_TPYE_PP;
    LED.GPIO_PinConfiguration.GPIO_PinPUPDControl=GPIO_PUPD_NO;

    BUTTON.pGPIOx =GPIOB;
    BUTTON.GPIO_PinConfiguration.GPIO_PinNumber=GPIO_PIN_NO_0;
    BUTTON.GPIO_PinConfiguration.GPIO_PinMode = GPIO_MODE_IT_FT;
    BUTTON.GPIO_PinConfiguration.GPIO_PinSpeed = GPIO_SPEED_HI;
    BUTTON.GPIO_PinConfiguration.GPIO_PinPUPDControl=GPIO_PUPD_PD;

    GPIO_PeriClkControl(GPIOB, ENABLE);
    GPIO_PeriClkControl(GPIOA, ENABLE);

    GPIO_Init(&LED);
    GPIO_Init(&BUTTON);
    // irq prioority
    GPIO_IRQPriorityConfig(IRQ_NO_EXTI0, GPIO_PR15);
    //IRQ confgiuring
    GPIO_IRQITConfig(IRQ_NO_EXTI0, ENABLE);


}
void EXTI0_IRQHandler ()
{
	delay(); // wait debouncing causing mulitple interupts
	GPIO_IRQHandling(GPIO_PIN_NO_0); //clear pending bi++++
	GPIO_ToggleOutputPin(GPIOA,GPIO_PIN_NO_8);
}
