/*
 * stm32f4xx_spi_driver.c
 *
 *  Created on: Dec 8, 2020
 *     Author: Ashwinder singh garcha
 */

#include "stm32f4xx_spi_driver.h"
#include <stdint.h>
#include <stdio.h>


static void spi_txe_interrupt_handle(SPI_Handle_t *pSPIHandle);
static void spi_Rxe_interrupt_handle(SPI_Handle_t *pSPIHandle);
static void spi_ovr_interrupt_handle(SPI_Handle_t *pSPIHandle);

/*
 *  peripheral Clock setup
 */
void SPI_PeriClockControl(SPI_regdef_t *pSPIx , uint8_t EnorDI)
{
	if (EnorDI == ENABLE)
	{
		if      (pSPIx == SPI1 ){SPI1_PERI_CLOCK_EN();}
		else if (pSPIx == SPI2 ){SPI2_PERI_CLOCK_EN();}
		else if (pSPIx == SPI3 ){SPI3_PERI_CLOCK_EN();}
		else if (pSPIx == SPI4 ){SPI4_PERI_CLOCK_EN();}
	}
	else
	{
		if      (pSPIx == SPI1 ){SPI1_PERI_CLOCK_DI();}
		else if (pSPIx == SPI2 ){SPI2_PERI_CLOCK_DI();}
		else if (pSPIx == SPI3 ){SPI3_PERI_CLOCK_DI();}
		else if (pSPIx == SPI4 ){SPI4_PERI_CLOCK_DI();}

	}



}

/*
 * Init or DE- init
 */
void SPI_Init(SPI_Handle_t *pSPIHandle)
{
	uint32_t tempreg =0 ;
	//1. mode master or slave

	// make CR1 register bit MSTR to one for master mode 0 for slave
		tempreg  |= (pSPIHandle->SPIConfig.SPI_DevideMode << SPI_CR1_MSTR); //set MSTR bit

	//2. bus_config
	       // full duplex register BIDIoe, BIDIMODE
	if      (pSPIHandle->SPIConfig.SPI_BusConfig == SPI_FULL_Dup)
	{
		  // when BIDIMODE 0: 2-line unidirectional data mode selected
		tempreg &= ~(1<<SPI_CR1_BIDIMOOE);
	}
	else if (pSPIHandle->SPIConfig.SPI_BusConfig == SPI_HALF_Dup)
	{
		  //when BIDIMODE  1: 1-line bidirectional data mode selected
		tempreg |= (1<<SPI_CR1_BIDIMOOE);
	}
    else if (pSPIHandle->SPIConfig.SPI_BusConfig == SPI_SIMPLEX_RXONLY)
    {
    	//when BIDIMODE  0: 1-line Unidirectional data mode selected
    	tempreg &= ~(1<<SPI_CR1_BIDIMOOE);
    	// bit RXonly use for simplex commm
    	tempreg |= (1<<SPI_CR1_RXONLY); //output disable

     }


	//3. frame format
	tempreg |= (pSPIHandle->SPIConfig.SPI_DFF << SPI_CR1_DFF);
	//4. clock speed
	tempreg |= (pSPIHandle->SPIConfig.SPI_SclkSpeed << SPI_CR1_BR  );
	//5. cpha
	tempreg |= (pSPIHandle->SPIConfig.SPI_CPHA << SPI_CR1_CPHA);
	//6  cpol
	tempreg |= (pSPIHandle->SPIConfig.SPI_CPOL << SPI_CR1_CPOL);
	//7. ssm
	tempreg |= (pSPIHandle->SPIConfig.SPI_SSM << SPI_CR1_SSM  );

	pSPIHandle->pSPIx->CR1 = tempreg; // finally move value to cr1

}


void SPI_DeInit(SPI_regdef_t *pSPIx )    //only need base address to rest in RCC regisater
{
	if      (pSPIx == SPI1) {SPI1_reset();}
	else if (pSPIx == SPI2) {SPI2_reset();}
	else if (pSPIx == SPI3) {SPI3_reset();}
	else if (pSPIx == SPI4) {SPI4_reset();}
}

/*
 * Data Send and Recieve
 */
// it is called blocking ex if len =1000 it block until all 1000 send out OR polling base
void SPI_SendData(SPI_regdef_t *pSPIx ,uint8_t *pTxBuffer ,uint32_t Len)
{
	while (Len>0)
	{
		//.wait until txe is 1
		while (!(pSPIx->SR & (SPI_TXE_FLAG ) ));   // wait when expression 0 or TXE 0
		//.check the dff
		if (pSPIx->CR1 & (1<<SPI_CR1_DFF ))
		{
			//16bit data copy to dr
			//decrement len by 2
			//incment buffer address
			pSPIx->DR = *((uint16_t*)pTxBuffer);   //make buffer 16 bit trick
			Len--;
			Len--;
			(uint16_t*)pTxBuffer++;                //point to next data item buffer increment by 2
		}else
		{
			//8bit data copy to dr
			//dec len by 1
			//incment buffer address
			pSPIx->DR = *(pTxBuffer);   //make buffer 8 bit trick
			Len--;
			pTxBuffer++;
		}
		//printf("SPI_DR pSPIx->DR: %lx\n",pSPIx->DR);
	}

}
void SPI_ReceiveData(SPI_regdef_t *pSPIx ,uint8_t *pRxBuffer ,uint32_t Len)
{
	while (Len>0)
	{
		while (!(pSPIx->SR & (1<<SPI_SR_RXNE)));          // wait until buffer non empty mean full
		if (pSPIx->CR1 & (1<<SPI_CR1_DFF ))               // if dff =1  //16bit
		{
			 *((uint16_t*)pRxBuffer) = pSPIx->DR ;        // load data from DR to rxbuffer  2 bytes
			Len--;
			Len--;
			(uint16_t*)pRxBuffer++;                        //incment rx buffer
		}else                                              //8bit
		{
			 *pRxBuffer = pSPIx->DR ;
			Len--;
			pRxBuffer++;

		}
	}

}



// non blocking api INTERUPT BASEd
uint8_t SPI_SendDataIT(SPI_Handle_t *pSPIHandle ,uint8_t *pTxBuffer ,uint32_t Len)
{
	uint8_t state = pSPIHandle->TxState;
	if (state != SPI_BUSY_IN_TX)
	{
		//1. save TX buffer address , len in global variable
		pSPIHandle->pTxBuffer =pTxBuffer;
		pSPIHandle->TxLen     =Len;
		//2. mark SPI busy so that no other code take over saeme spi until transmission is over
		pSPIHandle->TxState =SPI_BUSY_IN_TX;
		//3. enable TXEIE control bit to get interuppt whenver TXE flag is set in SR
		pSPIHandle->pSPIx->CR2 |= (1<<SPI_CR2_TXEIE);
		//4. data transfer handle by ISR code
	}

	return state;

}
uint8_t SPI_ReceiveDataIT(SPI_Handle_t *pSPIHandle ,uint8_t *pRxBuffer ,uint32_t Len)
{
	uint8_t state = pSPIHandle->RxState;
		if (state != SPI_BUSY_IN_RX)
		{
			//1. save TX buffer address , len in global variable
			pSPIHandle->pRxBuffer =pRxBuffer;
			pSPIHandle->RxLen     =Len;
			//2. mark SPI busy so that no other code take over saeme spi until transmission is over
			pSPIHandle->RxState =SPI_BUSY_IN_RX;
			//3. enable TXEIE control bit to get interuppt whenver TXE flag is set in SR
			pSPIHandle->pSPIx->CR2 |= (1<<SPI_CR2_RXNEIE );
			//4. data transfer handle by ISR code
		}

		return state;


}


/*
 * IRQ Configuration and ISR handling
 */
void SPI_IRQITConfig(uint8_t IRQNumber,uint8_t ENorDi)
{
	uint8_t regnumber;   // divide by 32 give u reg no like iser 0,1,2,3,4,5
	uint8_t bitlocation; // mod    by 32 give u bit loc bit 0----31

	// enable disable in nvic register
	if (ENorDi == ENABLE )
	{
		regnumber   = IRQNumber / 32 ; // give u reg number
		bitlocation = IRQNumber % 32 ; //
	//ex  IRQ 30  reg number   = 30 / 32 in int = 0 mean register ISER0
		        //bit location = 30 % 32 in int = 30 mean bit number 30
		// bit 30 of ISER0
		*(NVIC_ISER0 +4 * (regnumber)) |= (1<<bitlocation);   //setting bit
	}else
	{   //clearing IRQ INTERUPT
		regnumber   = IRQNumber /32 ; // give u reg number
		bitlocation = IRQNumber %32 ; //
	//ex  IRQ 30  reg number   = 30 / 32 in int = 0 mean register ICER0
		        //bit location = 30 % 32 in int = 30 mean bit number 30
		// bit 30 of ICER0
		*(NVIC_ICER0 +4 * (regnumber)) |= (1<<bitlocation); //clearing bit

	}

}
void SPI_IRQPriorityConfig(uint8_t IRQNumber,uint32_t IRQPriority)
{
	uint8_t temp1; //reg loc
	uint8_t temp2; //bit loc

	temp1   = IRQNumber / 4 ; // give u reg number
	temp2   = IRQNumber % 4 ; //bit loc
	uint8_t shiftamount;

	shiftamount = (temp2 * 8)+(8-NO_PR_BIT_IMPLEMENTED);

	*(NVIC_IPR0 + (temp1)) |= (IRQPriority<<shiftamount); //clearing bit

}



void SPI_IRQHandling(SPI_Handle_t *pHandle)
{
	//check which interupt occur
	if     ( (pHandle->pSPIx->SR & SPI_TXE_FLAG ) && (pHandle->pSPIx->CR2 & SPI_CR2_TXEIE)  )
	{
		//handle TXE
		spi_txe_interrupt_handle(pHandle);

	}
	else if( (pHandle->pSPIx->SR & SPI_RXE_FLAG ) && (pHandle->pSPIx->CR2 & SPI_CR2_RXNEIE)  )
	{
		//handle RXE
		spi_Rxe_interrupt_handle(pHandle);

	}
    else if( (pHandle->pSPIx->SR & SPI_SR_OVR  )   && (pHandle->pSPIx->CR2 & SPI_CR2_ERRIE )  )
    {
    	//overrun error and clearning OVR bit clear error
    	//handle ovr
    	spi_ovr_interrupt_handle(pHandle);
    }
}

/*
 * Other Peripheral Control APIs
 */

void SPI_PeripheralControl(SPI_regdef_t *pSPIx, uint8_t EnorDI)
{
	if (EnorDI == ENABLE)
	{
		pSPIx->CR1 |= (1<<SPI_CR1_SPE );   //enable
	}else
	{
		pSPIx->CR1 &= ~(1<<SPI_CR1_SPE );    // di
	}
}


void SPI_SSIConfig(SPI_regdef_t *pSPIx, uint8_t EnorDI)
{
	if (EnorDI == ENABLE)
	{
		pSPIx->CR1 |= (1<<SPI_CR1_SSI );   //enable SSI incase of SSM enable for pulling NSS to VCC internally
	}else
	{
		pSPIx->CR1 &= ~(1<<SPI_CR1_SSI);    // di
	}
}



void SPI_SSOEConfig(SPI_regdef_t *pSPIx, uint8_t EnorDI)
{
	if (EnorDI == ENABLE)
	{
		pSPIx->CR2 |= (1<<SPI_CR2_SSOE  );   //enable SSI incase of SSM enable for pulling NSS to VCC internally
	}else
	{
		pSPIx->CR2 &= ~(1<<SPI_CR2_SSOE );    // di
	}
}

uint8_t BUSY_flag(SPI_regdef_t *pSPIx)
{
	if(pSPIx->SR & (1<<SPI_SR_BSY ) )
	{
		return 1;
	}else
		return 0;
}



//some functions
static void spi_txe_interrupt_handle(SPI_Handle_t *pSPIHandle)
{
	if (pSPIHandle->pSPIx->CR1 & (1<<SPI_CR1_DFF ))
			{
				//16bit data copy to dr
				//decrement len by 2
				//incment buffer address
		        pSPIHandle->pSPIx->DR = *((uint16_t*)pSPIHandle->pTxBuffer);   //make buffer 16 bit trick
		        pSPIHandle->TxLen--;
		        pSPIHandle->TxLen--;;
				(uint16_t*)pSPIHandle->pTxBuffer++;                //point to next data item buffer increment by 2
			}else
			{
				//8bit data copy to dr
				//dec len by 1
				//incment buffer address
				pSPIHandle->pSPIx->DR = *(pSPIHandle->pTxBuffer);   //make buffer 8 bit trick
				pSPIHandle->TxLen--;
				pSPIHandle->pTxBuffer++;
			}
	if (!(pSPIHandle->TxLen))
	{
		//close spi if txlen = 0 and inform
		//tx over

		//this prevents interuupts from setting up of TXE flag
		SPI_CloseTranmission(pSPIHandle);
		SPI_ApplicationEventCallBack(pSPIHandle,SPI_EVENT_TX_CMPLT);

	}

}
static void spi_Rxe_interrupt_handle(SPI_Handle_t *pSPIHandle)
{
	if (pSPIHandle->pSPIx->CR1 & (1<<SPI_CR1_DFF ))
			{
				//16bit data copy to dr
				//decrement len by 2
				//incment buffer address
		         *((uint16_t*)pSPIHandle->pRxBuffer) = pSPIHandle->pSPIx->DR ;   //make buffer 16 bit trick
		        pSPIHandle->RxLen--;
		        pSPIHandle->RxLen--;;
				(uint16_t*)pSPIHandle->pRxBuffer--;                //point to next data item buffer increment by 2
			}else
			{
				//8bit data copy to dr
				//dec len by 1
				//incment buffer address
				 *(pSPIHandle->pRxBuffer) = pSPIHandle->pSPIx->DR ;   //make buffer 8 bit trick
				pSPIHandle->RxLen--;
				pSPIHandle->pRxBuffer--;
			}
	if (!(pSPIHandle->RxLen))
	{
		//close spi if txlen = 0 and inform
		//tx over

		//this prevents interuupts from setting up of TXE flag
		SPI_CloseReception(pSPIHandle);
		SPI_ApplicationEventCallBack(pSPIHandle,SPI_EVENT_RX_CMPLT);

	}
}
static void spi_ovr_interrupt_handle(SPI_Handle_t *pSPIHandle)
{
	//CLEAR OVR FLAG
	uint8_t temp;
	if(pSPIHandle->TxState != SPI_BUSY_IN_TX )
	{
		temp = pSPIHandle->pSPIx->DR;
	    temp = pSPIHandle->pSPIx->SR;
    }
	(void)temp;
	//INFORM APP
	SPI_ApplicationEventCallBack(pSPIHandle,SPI_EVENT_OVR_ERR);
}


void SPI_CloseTranmission(SPI_Handle_t *pSPIHandle)
{
	pSPIHandle->pSPIx->CR2 &= ~(1<<SPI_CR2_TXEIE );
	pSPIHandle->pTxBuffer = NULL;
	pSPIHandle->TxLen = 0;
	pSPIHandle->TxState = SPI_READY ;


}
void SPI_CloseReception(SPI_Handle_t *pSPIHandle)
{
	pSPIHandle->pSPIx->CR2 &= ~(1<<SPI_CR2_RXNEIE );
	pSPIHandle->pRxBuffer = NULL;
	pSPIHandle->RxLen = 0;
	pSPIHandle->RxState = SPI_READY ;

}

void SPI_ClearOVRflag(SPI_Handle_t *pSPIHandle)
{
	//CLEAR OVR FLAG
		uint8_t temp;
	temp = pSPIHandle->pSPIx->DR;
    temp = pSPIHandle->pSPIx->SR;
	(void)temp;
}

__attribute__((weak)) void SPI_ApplicationEventCallBack(SPI_Handle_t *pSPIHandle,uint8_t AppEv)
{
	//this is weak . app may override this
}
