/*
 * stm32f4xx_gpio_driver.c
 *
 *  Created on: Dec 3, 2020
 *       Author: Ashwinder singh garcha
 */


#include"stm32f4xx_gpio_driver.h"
/*
 * Peripheral Clock setUP
 */
/*****************************************************
 * @fn             -GPIO_Peripheral control
 *
 * @breif          -THIS FUNCTION ENABLE OR DISBALE PERIPHERAL CLOCK FOR GIVEN GPIO PORT
 *
 * @PARAM[IN]      -BASE ADDRESS OF GPIO PERIPHERAL
 * @PARAM[IN]      -ENABLE AND DISABLE MACROS
 * @PARAM[IN]      -
 *
 * @RETURN         -NONE
 *
 * @NOTE           -NONE
 */

void GPIO_PeriClkControl(GPIO_regdef_t *pGPIOx,uint8_t ENorDi)
{
	if (ENorDi == ENABLE)
	{
		     if(pGPIOx == GPIOA){GPIOA_PERI_CLOCK_EN();}
		else if(pGPIOx == GPIOB){GPIOB_PERI_CLOCK_EN();}
		else if(pGPIOx == GPIOC){GPIOC_PERI_CLOCK_EN();}
		else if(pGPIOx == GPIOD){GPIOD_PERI_CLOCK_EN();}
		else if(pGPIOx == GPIOE){GPIOE_PERI_CLOCK_EN();}
		else if(pGPIOx == GPIOF){GPIOF_PERI_CLOCK_EN();}
		else if(pGPIOx == GPIOG){GPIOG_PERI_CLOCK_EN();}
		else if(pGPIOx == GPIOH){GPIOH_PERI_CLOCK_EN();}
		else if(pGPIOx == GPIOI){GPIOI_PERI_CLOCK_EN();}
	}else
	{
		     if(pGPIOx == GPIOA){GPIOA_PERI_CLOCK_DI();}
		else if(pGPIOx == GPIOB){GPIOB_PERI_CLOCK_DI();}
		else if(pGPIOx == GPIOC){GPIOC_PERI_CLOCK_DI();}
		else if(pGPIOx == GPIOD){GPIOD_PERI_CLOCK_DI();}
		else if(pGPIOx == GPIOE){GPIOE_PERI_CLOCK_DI();}
		else if(pGPIOx == GPIOF){GPIOF_PERI_CLOCK_DI();}
		else if(pGPIOx == GPIOG){GPIOG_PERI_CLOCK_DI();}
		else if(pGPIOx == GPIOH){GPIOH_PERI_CLOCK_DI();}
		else if(pGPIOx == GPIOI){GPIOI_PERI_CLOCK_DI();}

	}
}


/*
*   Init and DE-Init
 */
/*****************************************************
 * @fn             -GPIO_Init
 *
 * @breif          -THIS FUNCTION SETUP ALL REGISTER OF GPIO PORT MODE,OTTPE,SPEED,PUPD,IDR,ODR,BSRR,LCKR,AFRL,AFRH
 *
 * @PARAM[IN]      -GPIO_Handle_t*PGPIOHandle
 * @PARAM[IN]      -
 * @PARAM[IN]      -
 *
 * @RETURN         -NONE
 *
 * @NOTE           -NONE
 */
void GPIO_Init(GPIO_Handle_t*PGPIOHandle)
{
	uint32_t temp=0;
	//1. CONFIGURE MODE OF GPIO PIN  INPUT,OUTPUT,ALTERNATE
	if (PGPIOHandle->GPIO_PinConfiguration.GPIO_PinMode <= GPIO_MODE_ANA)
	{
		//NON INTERUPT MODE
		temp = (PGPIOHandle->GPIO_PinConfiguration.GPIO_PinMode << (2*PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber));
		// IF PIN NUMBER =0
		// 2*0 = 0 THEN PUT VALUE OF MODE AT LLEFT SHIFTED TO 0
		//IF NUMBER =1
		// 2*1 = 2 THEN LEFT SHIFT THE MODE VALUE TO 2
		PGPIOHandle->pGPIOx->MODER &= ~(0X3<<(2*PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber));//CLEARING BITS
		PGPIOHandle->pGPIOx->MODER |= temp;  //STORE VALUE IN ACTUAL REGISTER OF MODE


	}else
	{
		if (PGPIOHandle->GPIO_PinConfiguration.GPIO_PinMode == GPIO_MODE_IT_FT)
		{
			// 1. CONFIGURE TRIGGER SELC FTSR REG
          EXTI->FTSR |= (1<<PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber);
          EXTI->RTSR &= ~(1<<PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber); //CLEAR rtsr
		}
		else if (PGPIOHandle->GPIO_PinConfiguration.GPIO_PinMode == GPIO_MODE_IT_RT)
		{
			// 1. CONFIGURE TRIGGER SELC RTSR REG
			EXTI->RTSR |= (1<<PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber);
			EXTI->FTSR &= ~(1<<PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber);//CLEAR FTSR
		}
		else if (PGPIOHandle->GPIO_PinConfiguration.GPIO_PinMode == GPIO_MODE_IT_RFT)
		{
			// 1. CONFIGURE TRIGGER SELC RTSR AND FTSR REG
			EXTI->RTSR |= (1<<PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber);
			EXTI->FTSR |= (1<<PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber);
		}

		//2.CONFIGURE SYSCFG_REG
        uint8_t temp1 = (PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber / 4);
        uint8_t temp2 = (PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber % 4);

        uint8_t portcode = GPIO_BASEADDR_TO_CODE(PGPIOHandle->pGPIOx);

        SYSCFG_CLOCK_EN();
       SYSCFG->EXTICR[temp1] = portcode << (temp2 *4);
		//3.EN USING EMI REG
		EXTI->IMR |= (1<<PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber);
	}

	//2. CONFIGURE SPEED
	temp =0;
	temp = ((PGPIOHandle->GPIO_PinConfiguration.GPIO_PinSpeed)<<(2*PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber));
	PGPIOHandle->pGPIOx->OSPEEDR &= ~(0X3<<(2*PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber));//CLEARING BITS
	PGPIOHandle->pGPIOx->OSPEEDR |= temp; //SETTING BITS

	//3. CONFIGURE PUPD
	temp =0;
	temp = ((PGPIOHandle->GPIO_PinConfiguration.GPIO_PinPUPDControl)<<(2*PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber));
	PGPIOHandle->pGPIOx->PUPDR &= ~(0X3<<(2*PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber));//CLEARING BITS
	PGPIOHandle->pGPIOx->PUPDR |= temp;

	//4. CONFIGURE OUTPUT TYPE
    temp=0;
    temp=((PGPIOHandle->GPIO_PinConfiguration.GPIO_PinOPTYPE)<<(1*PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber));
    PGPIOHandle->pGPIOx->OTYPER &= ~(0X1<<(1*PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber));//CLEARING BITS
    PGPIOHandle->pGPIOx->OTYPER |= temp;

    //5. CONFIGURE ALTERNATE MODE
    //ONLY WORK WHEN MODE = ALTERNATE

    temp=0;
    if(PGPIOHandle->GPIO_PinConfiguration.GPIO_PinMode ==GPIO_MODE_ALT)
    {
      if (PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber <= GPIO_PIN_NO_7)
       {
    	// USE LOW REGISTER
    	temp=(PGPIOHandle->GPIO_PinConfiguration.GPIO_PinALTFunMode)<<(4*PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber);
    	PGPIOHandle->pGPIOx->AFRL &= ~(0XF<<(4*PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber));//CLEARING BITS
    	PGPIOHandle->pGPIOx->AFRL |= temp;
       }else
       {
    	//USE HIGH REGISTER
    	temp=(PGPIOHandle->GPIO_PinConfiguration.GPIO_PinALTFunMode)<<(4*((PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber)-8));
    	PGPIOHandle->pGPIOx->AFRH &= ~(0XF<<(4*PGPIOHandle->GPIO_PinConfiguration.GPIO_PinNumber));//CLEARING BITS
    	PGPIOHandle->pGPIOx->AFRH |= temp;

        }
    }
}
/*****************************************************
 * @fn             -GPIO_DeInit
 *
 * @breif          -THIS FUNCTION RESET ALL REGISTER
 *
 * @PARAM[IN]      -GPIO_regdef_t *pGPIOx
 * @PARAM[IN]      -
 * @PARAM[IN]      -
 *
 * @RETURN         -NONE
 *
 * @NOTE           -NONE
 */
void GPIO_DeInit(GPIO_regdef_t *pGPIOx)
{
     if(pGPIOx == GPIOA){GPIOA_RESET() ;}
else if(pGPIOx == GPIOB){GPIOB_RESET() ;}
else if(pGPIOx == GPIOC){GPIOC_RESET() ;}
else if(pGPIOx == GPIOD){GPIOD_RESET() ;}
else if(pGPIOx == GPIOE){GPIOE_RESET() ;}
else if(pGPIOx == GPIOF){GPIOF_RESET() ;}
else if(pGPIOx == GPIOG){GPIOG_RESET() ;}
else if(pGPIOx == GPIOH){GPIOH_RESET() ;}
else if(pGPIOx == GPIOI){GPIOI_RESET() ;}

}


/*
 * DATA read and write
 */
uint8_t GPIO_ReadFromInputPin(GPIO_regdef_t *pGPIOx,uint8_t PinNumber)
{
	uint8_t value;
   value =(uint8_t)(( pGPIOx->IDR >>  ( PinNumber)) & 0x00000001);
   return value;
}
uint16_t GPIO_ReadFromInputPort(GPIO_regdef_t *pGPIOx)
{
	uint16_t value;
   value =(uint16_t) pGPIOx->IDR ;
   return value;

}
void GPIO_WritetoOutputPin(GPIO_regdef_t *pGPIOx,uint8_t PinNumber,uint8_t Value)
{
	if(Value == GPIO_PIN_SET )
		{
		   pGPIOx->ODR |= (1 << PinNumber);//SETTING
		}else
		{//RESETING
	      pGPIOx->ODR &= ~(1 << PinNumber);//CLEARING
		}
}
void GPIO_WritetoOutputPort(GPIO_regdef_t *pGPIOx,uint16_t Value)
{
	pGPIOx->ODR = Value;

}
void GPIO_ToggleOutputPin(GPIO_regdef_t *pGPIOx,uint8_t PinNumber)
{
	pGPIOx->ODR ^= (1<<PinNumber);
}
/*
 * IRQ CONFIG
 */
void GPIO_IRQITConfig(uint8_t IRQNumber,uint8_t ENorDi)
{
	if(ENorDi == ENABLE)
	{
		if    (IRQNumber <= 31)
			{
				//ISER0
			    *NVIC_ISER0 |= (1 << IRQNumber );
			}
			else if(IRQNumber >  31 && IRQNumber < 64 )
			{
				//ISER1
				*NVIC_ISER1 |= (1 << (IRQNumber % 32 ));
			}
			else if(IRQNumber >= 64 && IRQNumber < 96 )
			{
				//ISER2
				*NVIC_ISER2 |= (1 << (IRQNumber % 64 ));
			}


	}else
	{
		if    (IRQNumber <= 31)
		{
			//ICER0
			*NVIC_ICER0 |= (1 << IRQNumber );
		}
		else if(IRQNumber >  31 && IRQNumber < 64 )
		{
			//ICER1
			*NVIC_ICER1 |= (1 << (IRQNumber % 32 ));
		}
		else if(IRQNumber >= 64 && IRQNumber < 96 )
		{
			//ICER2
			*NVIC_ICER2 |= (1 << (IRQNumber % 64 ));
		}

	}

}


void GPIO_IRQPriorityConfig(uint8_t IRQNumber,uint32_t IRQPriority)
{
	// 1. find ipr register
	uint8_t iprx       =   IRQNumber / 4;
	uint8_t iprx_field =   IRQNumber % 4;   //

	uint8_t shift_amount = (iprx_field * 8) + (8 - NO_PR_BIT_IMPLEMENTED);
	// becz only higher 4 bit are using lower 4 bit are ignored
	*(NVIC_IPR0 + (iprx )) |= (IRQPriority << shift_amount);

	//*(NVIC_IPR0 + (iprx * 4)) |= (IRQPriority << (iprx_field * 8));  // very important
}


void GPIO_IRQHandling(uint8_t PinNumber)
{
	//implement isr function
	//clear EXTI PR register
	if (EXTI->PR & (1 << PinNumber ))
	{
		//clear
		EXTI->PR |= (1<< PinNumber);
	}

}
