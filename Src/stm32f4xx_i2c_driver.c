// Author: Ashwinder singh garcha

#include "stm32f4xx_i2c_driver.h"
#include <stdint.h>
#include <stdio.h>

static void I2C_GenerateStart(I2C_regdef_t*pI2Cx);
//static void I2C_GenerateStop(I2C_regdef_t*pI2Cx);
static void I2C_AddressPhaseWrite(I2C_regdef_t*pI2Cx,uint8_t SlaveAddr);
static void I2C_ClearADDR(I2C_Handle_t *pI2CHandle);
//void I2C_CloseSendData(I2C_Handle_t *pI2CHandle);
void I2C_CloseReceiveData(I2C_Handle_t *pI2CHandle);

/***************************************************************************
 * @fn					- I2C_PeriClockControl
 *
 * @brief				- Enables or disables clock for a given I2C
 *
 * @param[in]			- I2Cx base address
 * @param[in]			- "ENABLE" or "DISABLE" command for the I2Cx clock
 *
 * @return				- none
 *
 * @Note				- none
 */
void I2C_PeriClockControl(I2C_regdef_t*pI2Cx , uint8_t EnorDI)
{
	if (EnorDI == ENABLE)
	{
		if      (pI2Cx == I2C1){I2C1_PERI_CLOCK_EN();}
		else if (pI2Cx == I2C2){I2C2_PERI_CLOCK_EN();}
		else if (pI2Cx == I2C3){I2C3_PERI_CLOCK_EN();}
	}else
	{
		if      (pI2Cx == I2C1){I2C1_PERI_CLOCK_DI();}
		else if (pI2Cx == I2C2){I2C2_PERI_CLOCK_DI();}
		else if (pI2Cx == I2C3){I2C3_PERI_CLOCK_DI();}

	}

}
/***************************************************************************
 * @fn					- AckControl
 *
 * @brief				- Sets or clears ack bit
 *
 * @param[in]			- I2Cx base address
 * @param[in]			- "ENABLE" or "DISABLE" command for ack control
 *
 * @return				- none
 *
 * @Note				- none
 */
void I2C_ACKControl(I2C_regdef_t*pI2Cx, uint8_t EnorDI)
{
	if (EnorDI == ENABLE)
	{
		pI2Cx->CR1 |= (1 << I2C_CR1_ACK);

	}else
	{
		pI2Cx->CR1 &= ~(1 << I2C_CR1_ACK);
	}
}
/***************************************************************************
 * @fn					- I2C_GetFlagStatus
 *
 * @brief				- Fetches I2C_SR1 register flags
 *
 * @param[in]			- Register addresses of a given I2C
 * @param[in]			- Requested flag
 *
 * @return				- Flag status
 *
 * @Note				- none
 */

uint8_t I2C_GetFlagStatus(I2C_regdef_t*pI2Cx,uint32_t FlagName  )
{
	if (pI2Cx->SR1 & FlagName)
	{
		return FLAG_SET;
	}
	return FLAG_RESET;
}


//TO DO: implement PLL clock calculation
/***************************************************************************
 * @fn					- RCC_GetPLLOutputClock
 *
 * @brief				- Helper function to calculate PLL frequency
 *
 * @param[in]			- none
 *
 * @return				- PLL clock
 *
 * @Note				- none
 */
//static uint32_t RCC_PLLCLK()
//{
//	return 0;     // we are not configuring PLL
//}
/***************************************************************************
 * @fn					- RCC_GetPCLK1
 *
 * @brief				- Helper function to calculate I2C clock speed
 *
 * @param[in]			- none
 *
 * @return				- PCLK1
 *
 * @Note				- none
 */
//uint16_t AHBp[8]={2,4,8,16,64,128,256,512};
//uint8_t  APBp[4]={2,4,8,16};
//uint32_t RCC_GetPCLK1Value()
//{
//	uint32_t pclk1,systclk;
//	uint8_t clksrc,temp1,temp2,AHBpre,APBpre;
//
//	 clksrc = (((RCC->CFGR)>>2) & 0x3);
//	if      (clksrc == 0){systclk = 16000000;}       //HS1 rc oscillator
//	else if (clksrc == 1){systclk =  8000000;}       //HSE external 8mhz
//	else if (clksrc == 2){systclk =  RCC_PLLCLK();}  //pll
//
//
//	temp1 = (((RCC->CFGR) >>4) & 0xF);
//	if (temp1 <8){AHBpre = 1;}                      // clock div by 0
//	else{AHBpre =AHBp[temp1-8]; }                   // else div by 2,4,8,16,64,128,256,512
//
//	temp2 = (((RCC->CFGR) >>10) & 0x7);
//	if (temp2 <4){APBpre = 1;}                      // clock div by 0
//	else{APBpre =APBp[temp2-4]; }                   // else div by 2,4,8,16
//
//
//	pclk1 = ( (systclk / AHBpre) / APBpre);
//	return pclk1;
//}

/***************************************************************************
 * @fn					- I2C_Init
 *
 * @brief				- Configures I2C peripheral register given the
 * 						configuration
 *
 * @param[in]			- I2C structure that holds configuration and I2C address
 *
 * @return				- none
 *
 * @Note				- none
 */
void I2C_Init(I2C_Handle_t *pI2CHandle)
{
	uint32_t temp=0;

	//1.Enable the clock for I2Cx peripheral IN RCC

	I2C_PeriClockControl(pI2CHandle->pI2Cx , ENABLE);

	//2.FREQ BITS in CR2  with 16 mhz

	temp = (RCC_GetPCLK1Value()/1000000U);     //16000000/1000000 = 16 we only need value 16 010000 in hex
	pI2CHandle->pI2Cx->CR2 |=  (temp & 0x3F); // masking first 6 bits in   11 1111 = 3F
	temp=0;

	//3.DEVICE ADDRESS // enter by user


     temp = (pI2CHandle->I2CConfig.I2C_DeviceAddress << 1) ;            //address 7bit
     temp |= (1<<14); //Bit 14 Should always be kept at 1 by software.i dont know why
     pI2CHandle->pI2Cx->OAR1 |= temp ;
     temp =0;
     // bit 15 ADDMODE =0 use as 7 bit // if u want ton configure 10 bit adrress set this bit
     // in our case we dont need to configure this bit
     // we only use 7 bit address


  	//4.SCLK CLOCK SPEED
 		 //			#define I2C_SCL_SPEED_SM   100000            //CCR reg  100khz  bit ccr
 		 //			#define I2C_SCL_SPEED_FM4k 400000            //CCR reg  400khz
 		 //			#define I2C_SCL_SPEED_FM2k 200000            //CCR reg  200khz

      uint16_t ccr_val =0;
 	  if (pI2CHandle->I2CConfig.I2C_SclkSpeed <= I2C_SCL_SPEED_SM )
 	  {
 		  //configure Standard mode
 		  //bit 15 already 0 we need not to configure F/S
 		 ccr_val =  (RCC_GetPCLK1Value() / (2 * pI2CHandle->I2CConfig.I2C_SclkSpeed));
 		 //           16000000           /  2 * 100000  = 80 in hex is 0x50
 		 temp |= (ccr_val & 0xFFF);  //Mask bits so we don't overwrite
 	  }
 	  else  //Fast mode
 	  {
 		  //bit 15  we need  to configure F/S
 		  temp |= (1<<15);
 		  temp |=(pI2CHandle->I2CConfig.I2C_FMDutyCycle<<14);
 		  //configure fast mode
 		  //1.mode duty = 0  speed 200000
 		  if (pI2CHandle->I2CConfig.I2C_FMDutyCycle ==I2C_DUTY_2 )
 		  {
 			  ccr_val =  (RCC_GetPCLK1Value() / (3 * pI2CHandle->I2CConfig.I2C_SclkSpeed));
 		  }
 		  else
 		  //2.mode duty = 1  speed 400000
 		  {
 			  ccr_val =  (RCC_GetPCLK1Value() / (25 * pI2CHandle->I2CConfig.I2C_SclkSpeed));

 	      }
 		  temp |= (ccr_val & 0xFFF); // move value in CCR register


 	     //4.	FMDutyCycle
 				//			#define I2C_DUTY_2      0                     // reg CCR bit DUTY bit 14
 				//			#define I2C_DUTY_16_9   1
 	  }

 	  pI2CHandle->pI2Cx->CCR = temp;
 	  temp=0;

     //5. TRISE configuration
	     //Maximum rise time-=>These bits must be programmed with the maximum SCL rise time given in the I2C bus
		//specification, incremented by 1 (see RM0090 reference manual)
	  if (pI2CHandle->I2CConfig.I2C_SclkSpeed <= I2C_SCL_SPEED_SM )
	  {
		  //configure Standard mode
          temp=0;
		  temp = (RCC_GetPCLK1Value() / 1000000U ) +1; //1000ns = 1microsecond or /1mhz


	  }
	  else
	  {
		  //configure fast mode
		  temp = ((RCC_GetPCLK1Value() *300 ) /1000000000U) +1; //300ns = 1microsecond or /1mhz


	  }
	  //pI2CHandle->pI2Cx->TRISE=0;

	  pI2CHandle->pI2Cx->TRISE |= (temp & 0x3f); // mask first 6 bit
	  temp=0;

	  //1.	ACKControl
//	      //			#define I2C_ACK_ENABLE    1                    // reg cr1 bit ack
//	      //			#define I2C_ACK_DISABLE   0
//		temp |= (pI2CHandle->I2CConfig.I2C_ACKControl << I2C_CR1_ACK);
//		pI2CHandle->pI2Cx->CR1 |= temp;

}

//DE- init //only need base address to rest in RCC regisater
void I2C_DeInit(I2C_regdef_t*pI2Cx )
{
	if      (pI2Cx == I2C1){I2C1_reset();}
	else if (pI2Cx == I2C2){I2C2_reset();}
	else if (pI2Cx == I2C3){I2C3_reset();}

}
/***************************************************************************
 * @fn					- I2C_PeripheralControl
 *
 * @brief				- Enables or disables a given I2C peripheral
 *
 * @param[in]			- Register addresses of a given I2C
 * @param[in]			- Enable or disable command
 *
 * @return				- none
 *
 * @Note				- none
 */
void I2C_PeripheralControl(I2C_regdef_t*pI2Cx, uint8_t EnorDI)
{
	if (EnorDI == ENABLE)
	{
		pI2Cx->CR1 |= (1 << I2C_CR1_PE);
		I2C_ACKControl(pI2Cx,ENABLE);        //ACK ENABLE AFTER PE ******INPORTANT********

	}else
	{
		pI2Cx->CR1 &= ~(1 << I2C_CR1_PE);
	}

}

/*
 * Data Send and Recieve
 */

/***************************************************************************
 * @fn					- I2C_GenerateStart
 *
 * @brief				- Helper function to initiate I2C communications
 *
 * @param[in]			- Register addresses of a given I2C
 *
 * @return				- none
 *
 * @Note				- none
 */
static void I2C_GenerateStart(I2C_regdef_t*pI2Cx)
{
	//setting bit START
	pI2Cx->CR1 |= (1<<I2C_CR1_START);

}

/***************************************************************************
 * @fn					- I2C_GenerateStop
 *
 * @brief				- Helper function to produce stop condition
 *
 * @param[in]			- Register addresses of a given I2C
 *
 * @return				- none
 *
 * @Note				- none
 */
 void I2C_GenerateStop(I2C_regdef_t*pI2Cx)
{
	//setting bit STop
	pI2Cx->CR1 |= (1<<I2C_CR1_STOP);

}
/***************************************************************************
 * @fn					- I2C_AddressPhaseRead
 *
 * @brief				- Helper function to initiate address phase of I2C
 * 						communications when in reception
 *
 * @param[in]			- Register addresses of a given I2C
 * @param[in]			- Address of the slave
 *
 * @return				- none
 *
 * @Note				- none
 */
static void I2C_AddressPhaseRead(I2C_regdef_t*pI2Cx,uint8_t SlaveAddr)
{
	SlaveAddr = SlaveAddr << 1;  //SlaveAddr is Slave address
	SlaveAddr |= 1;              // read + r/nw bit=1
	pI2Cx->DR = SlaveAddr;
}
/***************************************************************************
 * @fn					- I2C_AddressPhaseWrite
 *
 * @brief				- Helper function to initiate address phase of I2C
 * 						communications when in transmission
 *
 * @param[in]			- Register addresses of a given I2C
 * @param[in]			- Address of the slave
 *
 * @return				- none
 *
 * @Note				- none
 */
static void I2C_AddressPhaseWrite(I2C_regdef_t*pI2Cx,uint8_t SlaveAddr)
{
	SlaveAddr = SlaveAddr << 1;
	SlaveAddr &= ~(1<<0); //SlaveAddr is Slave address + Write r/nw bit=0
	pI2Cx->DR = SlaveAddr;
}
/***************************************************************************
 * @fn					- I2C_ClearADDR
 *
 * @brief				- Helper function to clear ADDR flag
 *
 * @param[in]			- I2C structure that holds configuration and I2C address
 *
 * @return				- none
 *
 * @Note				- ADDR flag is cleared when SR1 is read followed
 * 						by SR2 is read operations or by hardware when PE
 * 						flag is 0 (see reference manual: RM0090)
 */
static void I2C_ClearADDR(I2C_Handle_t *pI2CHandle)
{
	uint32_t foo;                              //dummy variable
//	foo = pI2CHandle->pI2Cx->SR1;
//	foo = pI2CHandle->pI2Cx->SR2;
//	(void) foo;
		if (pI2CHandle->pI2Cx->SR2 & (1 << I2C_SR2_MSL)){
			if (pI2CHandle->TxRxState == I2C_BUSY_IN_RX && pI2CHandle->RxSize == 1) {
			//Disable ack

		     I2C_ACKControl(pI2CHandle->pI2Cx, DISABLE);

			//Clear ADDR
			foo = pI2CHandle->pI2Cx->SR1;
			foo = pI2CHandle->pI2Cx->SR2;
			(void) foo; //Cast it to void to avoid compiler warning

		} else {
			//Clear ADDR in slave mode
			foo = pI2CHandle->pI2Cx->SR1;
			foo = pI2CHandle->pI2Cx->SR2;
			(void) foo; //Cast it to void to avoid compiler warning
			}
		}          //type cast with void otherwise compiler give unused err
}
/***************************************************************************
 * @fn					- I2C_MasterSendData
 *
 * @brief				- Sending data over a given I2C peripheral
 *
 * @param[in]			- I2C structure that holds configuration and I2C address
 * @param[in]			- Buffer that holds the data to be sent
 * @param[in]			- Total byte count that will be sent
 * @param[in]			- Address of the device that master will convey it's message to
 *
 * @return				- none
 *
 * @Note				- none
 */
void I2C_MasterSendData(I2C_Handle_t *pI2CHandle,uint8_t *pTxBuffer,uint32_t Len , uint8_t SlaveAddr,uint8_t Sr)
{
//----------------------------------------------start condition S------------------------------------------------------
	//1.generate start condition
	I2C_GenerateStart(pI2CHandle->pI2Cx);

//----------------------------------------------EV5------------------------------------------------------
	//2.EV5 by checking SB flag in reg SR1 : note SB strech scl clk unitl cleared // only clear write address to DR
	while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx,I2C_FLAG_SB ));
	    //.clear by write address to DR send slave adress with R/W set = 0 for write operation total 8 bit
//	uint32_t dummyRead;
//		dummyRead= pI2CHandle->pI2Cx->SR1;
	I2C_AddressPhaseWrite(pI2CHandle->pI2Cx,SlaveAddr);

//----------------------------------------------EV6------------------------------------------------------
	//3.EV6 ack rx tocheck ack ADDR flag set wait until ADDR flag set
	while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx,I2C_FLAG_ADDR  ));
	//.clear by read of the SR1 register followed by a read of the SR2
	I2C_ClearADDR(pI2CHandle);

//----------------------------------------------EV8_1------------------------------------------------------
	while(Len > 0)
    {
    	while(!I2C_GetFlagStatus(pI2CHandle->pI2Cx , I2C_FLAG_TxE )); //wait until txe set
    	pI2CHandle->pI2Cx->DR = *pTxBuffer;
    	pTxBuffer++;
//		while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_TxE)) {} //Check if transmission data register is empty
//		pI2CHandle->pI2Cx->DR = *pTxBuffer; //Write to data register
//		pTxBuffer++; //Increment data buffer

    	Len--;
    }
//----------------------------------------------EV8_2------------------------------------------------------
    //wait unitl txe=1,bte=1
    while(!I2C_GetFlagStatus(pI2CHandle->pI2Cx,I2C_FLAG_TxE )){;} //wait for txe
    while(!I2C_GetFlagStatus(pI2CHandle->pI2Cx,I2C_FLAG_BTF )){;} //wait for bte

//----------------------------------------------stop condition P------------------------------------------------------
    if (Sr == I2C_DISABLE_SR)
    {//1.generate stop condition

    	I2C_GenerateStop(pI2CHandle->pI2Cx);
    }
}

/***************************************************************************
 * @fn					- I2C_MasterReceiveData
 *
 * @brief				- Sending receving over a given I2C peripheral
 *
 * @param[in]			- I2C structure that holds configuration and I2C address
 * @param[in]			- Buffer that holds the data rx
 * @param[in]			- Total byte count that will be received
 * @param[in]			- Address of the device that master will convey it's message to
 *
 * @return				- none
 *
 * @Note				- none
 */
void I2C_MasterReceiveData(I2C_Handle_t *pI2CHandle,uint8_t *pRxBuffer,uint32_t Len , uint8_t SlaveAddr,uint8_t Sr)
{
	//1.genrate start condition
	I2C_GenerateStart(pI2CHandle->pI2Cx);

	//2.check SB flag for set
	while(!I2C_GetFlagStatus(pI2CHandle->pI2Cx,I2C_FLAG_SB) );

	//3.WRITE address of slave and r/w bit in read mode 1 which clear SB flag
	I2C_AddressPhaseRead(pI2CHandle->pI2Cx,SlaveAddr);

	//4. wait for ACK from slave check byADDR =1 and clear by reading SR1,SR2
	while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx,I2C_FLAG_ADDR  ));
	//In case of the reception of 1 byte,
	     //the Acknowledge disable ACK=0 ,stop bit =1 must be performed during EV6 event, i.e. before clearing ADDR flag
	     //after the reception of 1 byte NACK WILL be activate becz we disbale in last step
	//read data from DR only when RXNE set


	//procedure to read only 1 byte from slave
	if(Len == 1)
	{
		//disable ACK
		 I2C_ACKControl(pI2CHandle->pI2Cx, DISABLE);

		 //clear ADDRF flag
		 I2C_ClearADDR(pI2CHandle);

		 //wait until  RXNE becomes 1
		 while(!I2C_GetFlagStatus(pI2CHandle->pI2Cx,I2C_FLAG_RxNE));

		 if (Sr == I2C_DISABLE_SR)
		    {//1.generate stop condition

		    	I2C_GenerateStop(pI2CHandle->pI2Cx);
		    }

		 //read LAST DATA in buffer
		 *pRxBuffer = pI2CHandle->pI2Cx->DR;
	}



	if(Len > 1)
	{
		I2C_ACKControl(pI2CHandle->pI2Cx, ENABLE); /// important here ack should disable only at second last bit
		//clear ADDRF flag
		I2C_ClearADDR(pI2CHandle);

		//2.Start reception
		for (uint32_t i = Len; i > 0; i--)
		   {
			   //Wait until RxNE is 1
			   while(!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_RxNE));

			   if (i == 2)
			   { //When there is 2 bytes remaining close I2C reception

				   //Disable ack before receiving last byte
				   I2C_ACKControl(pI2CHandle->pI2Cx, DISABLE);

				    if (Sr == I2C_DISABLE_SR)
				    {//1.generate stop condition

				    	I2C_GenerateStop(pI2CHandle->pI2Cx);
				    }

			    }

			 //3.Read the data from data register
			 *pRxBuffer = pI2CHandle->pI2Cx->DR;

			 //4.Increment buffer address
			 pRxBuffer++;
			// Len--;

		   }

//		//now reenable ACK
//		I2C_ACKControl(pI2CHandle->pI2Cx, ENABLE);



	}

}

/***************************************************************************
                              NON BLOCKING API WITH IT

*/

/***************************************************************************
 * @fn					- I2C_MasterSendDataIT
 *
 * @brief				- Sending data over a given I2C peripheral
 *
 * @param[in]			- I2C structure that holds configuration and I2C address
 * @param[in]			- Buffer that holds the data to be sent
 * @param[in]			- Total byte count that will be sent
 * @param[in]			- Address of the device that master will convey it's message to
 *
 * @return				- none
 *
 * @Note				- none
 */
uint8_t I2C_MasterSendDataIT(I2C_Handle_t *pI2CHandle,uint8_t *pTxBuffer,uint32_t Len , uint8_t SlaveAddr,uint8_t Sr)
{
	uint8_t busystate = pI2CHandle->TxRxState;
	if ((busystate !=I2C_BUSY_IN_TX ) && (busystate != I2C_BUSY_IN_RX))   //ie = ready not busy in tx or rx
	{
		pI2CHandle->pTxBuffer =pTxBuffer     ;
		pI2CHandle->TxLen     =Len           ;
		pI2CHandle->TxRxState =I2C_BUSY_IN_TX;
		pI2CHandle->DevAddr   =SlaveAddr     ;
		pI2CHandle->Sr        =Sr            ;

		//Implement code to Generate START Condition
		I2C_GenerateStart(pI2CHandle->pI2Cx);

		//Implement the code to enable ITBUFEN Control Bit
		pI2CHandle->pI2Cx->CR2 |= ( 1 << I2C_CR2_ITBUFEN );

		//Implement the code to enable ITEVFEN Control Bit
		pI2CHandle->pI2Cx->CR2 |= ( 1 << I2C_CR2_ITEVTEN );

		//Implement the code to enable ITERREN Control Bit
		pI2CHandle->pI2Cx->CR2 |= ( 1 << I2C_CR2_ITERREN );
	}

	return busystate;
}
/***************************************************************************
 * @fn					- I2C_MasterReceiveDataIT
 *
 * @brief				- Sending receving over a given I2C peripheral
 *
 * @param[in]			- I2C structure that holds configuration and I2C address
 * @param[in]			- Buffer that holds the data rx
 * @param[in]			- Total byte count that will be received
 * @param[in]			- Address of the device that master will convey it's message to
 *
 * @return				- none
 *
 * @Note				- none
 */
uint8_t I2C_MasterReceiveDataIT(I2C_Handle_t *pI2CHandle,uint8_t *pRxBuffer,uint32_t Len , uint8_t SlaveAddr,uint8_t Sr)
{
	uint8_t busystate = pI2CHandle->TxRxState;

	if( (busystate != I2C_BUSY_IN_TX) && (busystate != I2C_BUSY_IN_RX))
	{
		pI2CHandle->pRxBuffer = pRxBuffer;
		pI2CHandle->RxLen     = Len;
		pI2CHandle->TxRxState = I2C_BUSY_IN_RX;
		pI2CHandle->RxSize    = Len;              //Rxsize is used in the ISR code to manage the data reception
		pI2CHandle->DevAddr   = SlaveAddr;
		pI2CHandle->Sr        = Sr;

		//Implement code to Generate START Condition
		I2C_GenerateStart(pI2CHandle->pI2Cx);

		//Implement the code to enable ITBUFEN Control Bit
		pI2CHandle->pI2Cx->CR2 |= ( 1 << I2C_CR2_ITBUFEN );

		//Implement the code to enable ITEVFEN Control Bit
		pI2CHandle->pI2Cx->CR2 |= ( 1 << I2C_CR2_ITEVTEN );

		//Implement the code to enable ITERREN Control Bit
		pI2CHandle->pI2Cx->CR2 |= ( 1 << I2C_CR2_ITERREN );

	}

	return busystate;

}
/***************************************************************************
 * @fn					- I2C_IRQITConfig
 *
 * @brief				- Sending receving over a given I2C peripheral
 *
 * @param[in]			-
 * @param[in]			-
 * @param[in]			-
 * @param[in]			-
 *
 * @return				- none
 *
 * @Note				- none
 */
void I2C_IRQITConfig(uint8_t IRQNumber,uint8_t ENorDi)
{
	uint8_t regnumber;   // divide by 32 give u reg no like iser 0,1,2,3,4,5
	uint8_t bitlocation; // mod    by 32 give u bit loc bit 0----31

	// enable disable in nvic register
	if (ENorDi == ENABLE )
	{
		regnumber   = IRQNumber / 32 ; // give u reg number
		bitlocation = IRQNumber % 32 ; //
	//ex  IRQ 30  reg number   = 30 / 32 in int = 0 mean register ISER0
		        //bit location = 30 % 32 in int = 30 mean bit number 30
		// bit 30 of ISER0
		*(NVIC_ISER0 +4 * (regnumber)) |= (1<<bitlocation);   //setting bit
	}else
	{   //clearing IRQ INTERUPT
		regnumber   = IRQNumber /32 ; // give u reg number
		bitlocation = IRQNumber %32 ; //
	//ex  IRQ 30  reg number   = 30 / 32 in int = 0 mean register ICER0
		        //bit location = 30 % 32 in int = 30 mean bit number 30
		// bit 30 of ICER0
		*(NVIC_ICER0 +4 * (regnumber)) |= (1<<bitlocation); //clearing bit

	}

}
/***************************************************************************
 * @fn					- I2C_IRQPriorityConfig
 *
 * @brief				- Sending receving over a given I2C peripheral
 *
 * @param[in]			-
 * @param[in]			-
 * @param[in]			-
 * @param[in]			-
 *
 * @return				- none
 *
 * @Note				- none
 */
void I2C_IRQPriorityConfig(uint8_t IRQNumber,uint32_t IRQPriority)
{
	uint8_t temp1; //reg loc
	uint8_t temp2; //bit loc

	temp1   = IRQNumber / 4 ; // give u reg number
	temp2   = IRQNumber % 4 ; //bit loc
	uint8_t shiftamount;

	shiftamount = (temp2 * 8)+(8-NO_PR_BIT_IMPLEMENTED);

	*(NVIC_IPR0 + (temp1)) |= (IRQPriority<<shiftamount); //clearing bit

}
/***************************************************************************
 * @fn					- IRQHandling
 *
 * @brief				- Sending receving over a given I2C peripheral
 *
 * @param[in]			-
 * @param[in]			-
 * @param[in]			-
 * @param[in]			-
 *
 * @return				- none
 *
 * @Note				- none
 */
void I2C_EV_IRQHandling(I2C_Handle_t *pI2CHandle)
{
	//Interrupt handling for both master and slave mode of a device
    uint8_t temp1,temp2,temp3;
	temp1 = pI2CHandle->pI2Cx->CR2 & (1<< I2C_CR2_ITEVTEN  );  //check CR2 EVT enable
	temp2 = pI2CHandle->pI2Cx->CR2 & (1<< I2C_CR2_ITBUFEN  );  //check CR2 BUF enable
	temp3 = pI2CHandle->pI2Cx->SR1 & (1<< I2C_SR1_SB       );  //check SR2 SB  event

	//1. Handle For interrupt generated by SB event
	//	Note : SB flag is only applicable in Master mode
   if (temp1  && temp3)
   {
	   //SB flag is set only used in master mode
	   if     (pI2CHandle->TxRxState == I2C_BUSY_IN_TX)
	   {
		   I2C_AddressPhaseWrite(pI2CHandle->pI2Cx, pI2CHandle->DevAddr);
	   }
	   else if (pI2CHandle->TxRxState == I2C_BUSY_IN_RX)
	   {
		   I2C_AddressPhaseRead(pI2CHandle->pI2Cx, pI2CHandle->DevAddr);
	   }
   }


   temp3 = pI2CHandle->pI2Cx->SR1 & (1<< I2C_SR1_ADDR       );  //check SR2 ADDR event
	//2. Handle For interrupt generated by ADDR event
	//Note : When master mode : Address is sent
	//		 When Slave mode   : Address matched with own address
   if (temp1  && temp3)
   {
	   //ADDR event
       I2C_ClearADDR(pI2CHandle);   // clear ADDR
   }


   temp3 = pI2CHandle->pI2Cx->SR1 & (1<< I2C_SR1_BTF       );  //check SR2 SB  event
	//3. Handle For interrupt generated by BTF(Byte Transfer Finished) event
   if (temp1  && temp3)
   {
	   //BTF event use to end transmission
	   if (pI2CHandle->TxRxState == I2C_BUSY_IN_TX)
	   {
		   // make sure TXE set
		   if (pI2CHandle->pI2Cx->SR1 & (1<< I2C_SR1_TxE       ))
				   {
			          // now btf , txe both set close transmission
			           if (pI2CHandle->TxLen ==0)
			              {

			        	        //1.generate stop condition
								if (pI2CHandle->Sr == I2C_DISABLE_SR)
								{
								  I2C_GenerateStop(pI2CHandle->pI2Cx);
								}

								//2reset all mem element of handle struct
								I2C_CloseSendData(pI2CHandle);

								//3notify application About transmission complete
								I2C_ApplicationEventCallBack(pI2CHandle, I2C_EV_TX_CMPLT);
			               }
				   }
	   }
	   else if (pI2CHandle->TxRxState == I2C_BUSY_IN_RX)
	   {
		   //no use in case of RX
	   }
   }

   temp3 = pI2CHandle->pI2Cx->SR1 & (1<< I2C_SR1_STOPF       );  //check SR2 STOPF event
	//4. Handle For interrupt generated by STOPF event
	// Note : Stop detection flag is applicable only slave mode . For master this flag will never be set
   if (temp1  && temp3)
   {
	   //STOPF event
	   //clear stop flag (i.e read SR1 and write to CR1)
	   pI2CHandle->pI2Cx->CR1 |= 0x0000;  //dont write any thing otherwise it change settings
		//3notify application About transmission complete
		I2C_ApplicationEventCallBack(pI2CHandle, I2C_EV_STOP_CMPLT);
   }

   temp3 = pI2CHandle->pI2Cx->SR1 & (1<< I2C_SR1_TxE         );  //check SR2 TXE event
	//5. Handle For interrupt generated by TXE event
   if (temp1  && temp2 && temp3)
   {   //only do if master mode MSL bit in SR1
	   if (pI2CHandle->pI2Cx->SR1 & (1<<I2C_SR2_MSL))
	   {
	   //TXE event
	   //data transmission
	   if (pI2CHandle->TxRxState == I2C_BUSY_IN_TX)
	   {
		   //now only transmitting data
		   if (pI2CHandle->TxLen >0)
		   {
			    //1.load data in dr
			   pI2CHandle->pI2Cx->DR = *(pI2CHandle->pTxBuffer);
				//2.decrement txlen
			   pI2CHandle->TxLen--;
				//3.inc buffer addres
			   pI2CHandle->pTxBuffer++;
		    }
	     }
	   }
   }

   temp3 = pI2CHandle->pI2Cx->SR1 & (1<< I2C_SR1_RxNE        );  //check SR2 RXNE event
	//6. Handle For interrupt generated by RXNE event
		if (temp1 && temp2 && temp3) {
			//only do if master mode MSL bit in SR1
			if (pI2CHandle->pI2Cx->SR1 & (1 << I2C_SR2_MSL)) {
				//RXNE event
				if (pI2CHandle->TxRxState == I2C_BUSY_IN_RX) {
					//we have to do data reception for  1 byte
					if (pI2CHandle->RxSize == 1) {

						//read data to buffer
						*pI2CHandle->pRxBuffer = pI2CHandle->pI2Cx->DR;
						pI2CHandle->RxLen--;
					}

				}
				//we have to do data reception for >1 byte
				if (pI2CHandle->RxSize > 1) {
					if (pI2CHandle->RxLen == 2) {
						//clear ack bit
						I2C_ACKControl(pI2CHandle->pI2Cx, DISABLE);

					}
					//read DR
					*pI2CHandle->pRxBuffer = pI2CHandle->pI2Cx->DR;
					pI2CHandle->pRxBuffer++;
					pI2CHandle->RxLen--;
				}
				if (pI2CHandle->RxLen == 0) {
					//close I2C
					//1.generate STOP
					if (pI2CHandle->Sr == I2C_DISABLE_SR) {	//1.generate stop condition

						I2C_GenerateStop(pI2CHandle->pI2Cx);
					}
					//2.close I2C rx
					I2C_CloseReceiveData(pI2CHandle);
					//3.notify app
					I2C_ApplicationEventCallBack(pI2CHandle, I2C_EV_RX_CMPLT);
				}
			}
		}
}


/*********************************************************************
 * @fn      		  - I2C_ER_IRQHandling
 *
 * @brief             -
 *
 * @param[in]         -
 * @param[in]         -
 * @param[in]         -
 *
 * @return            -
 *
 * @Note              - Complete the code also define these macros in the driver
						header file
						#define I2C_ERROR_BERR  3
						#define I2C_ERROR_ARLO  4
						#define I2C_ERROR_AF    5
						#define I2C_ERROR_OVR   6
						#define I2C_ERROR_TIMEOUT 7
 */
void I2C_ER_IRQHandling(I2C_Handle_t *pI2CHandle)
{
	uint32_t temp1,temp2;

	    //Know the status of  ITERREN control bit in the CR2
		temp2 = (pI2CHandle->pI2Cx->CR2) & ( 1 << I2C_CR2_ITERREN);


	/***********************Check for Bus error************************************/
		temp1 = (pI2CHandle->pI2Cx->SR1) & ( 1<< I2C_SR1_BERR);
		if(temp1  && temp2 )
		{
			//This is Bus error

			//Implement the code to clear the buss error flag
			pI2CHandle->pI2Cx->SR1 &= ~( 1 << I2C_SR1_BERR);

			//Implement the code to notify the application about the error
		 I2C_ApplicationEventCallBack(pI2CHandle, I2C_ERROR_BERR );

		}

	/***********************Check for arbitration lost error************************************/
		temp1 = (pI2CHandle->pI2Cx->SR1) & ( 1 << I2C_SR1_ARLO );
		if(temp1  && temp2)
		{
			//This is arbitration lost error

			//Implement the code to clear the arbitration lost error flag
			pI2CHandle->pI2Cx->SR1 &= ~( 1 << I2C_SR1_ARLO );
			//Implement the code to notify the application about the error

			 I2C_ApplicationEventCallBack(pI2CHandle,I2C_ERROR_ARLO);
		}

	/***********************Check for ACK failure  error************************************/

		temp1 = (pI2CHandle->pI2Cx->SR1) & ( 1 << I2C_SR1_AF);
		if(temp1  && temp2)
		{
			//This is ACK failure error

		    //Implement the code to clear the ACK failure error flag
			pI2CHandle->pI2Cx->SR1 &= ~( 1 <<I2C_SR1_AF );
			//Implement the code to notify the application about the error
			 I2C_ApplicationEventCallBack(pI2CHandle,I2C_ERROR_AF);
		}

	/***********************Check for Overrun/underrun error************************************/
		temp1 = (pI2CHandle->pI2Cx->SR1) & ( 1 << I2C_SR1_OVR);
		if(temp1  && temp2)
		{
			//This is Overrun/underrun

		    //Implement the code to clear the Overrun/underrun error flag
			pI2CHandle->pI2Cx->SR1 &= ~( 1 <<I2C_SR1_OVR );
			//Implement the code to notify the application about the error
			 I2C_ApplicationEventCallBack(pI2CHandle,I2C_ERROR_OVR);
		}

	/***********************Check for Time out error************************************/
		temp1 = (pI2CHandle->pI2Cx->SR1) & ( 1 << I2C_SR1_TIMEOUT);
		if(temp1  && temp2)
		{
			//This is Time out error

		    //Implement the code to clear the Time out error flag
			pI2CHandle->pI2Cx->SR1 &= ~( 1 <<I2C_SR1_TIMEOUT );
			//Implement the code to notify the application about the error
			 I2C_ApplicationEventCallBack(pI2CHandle,I2C_ERROR_TIMEOUT);
		}
}
void I2C_CloseSendData(I2C_Handle_t *pI2CHandle)
{
	//Implement the code to disable ITBUFEN Control Bit
	pI2CHandle->pI2Cx->CR2 &= ~( 1 << I2C_CR2_ITBUFEN);

	//Implement the code to disable ITEVFEN Control Bit
	pI2CHandle->pI2Cx->CR2 &= ~( 1 << I2C_CR2_ITEVTEN);


	pI2CHandle->TxRxState = I2C_READY;
	pI2CHandle->pTxBuffer = NULL;
	pI2CHandle->TxLen = 0;

}
void I2C_CloseReceiveData(I2C_Handle_t *pI2CHandle)
{
	//dis all enable interruppt
	pI2CHandle->pI2Cx->CR2 &= ~(1<< I2C_CR2_ITEVTEN );
	pI2CHandle->pI2Cx->CR2 &= ~(1<< I2C_CR2_ITBUFEN  );

	pI2CHandle->TxRxState = I2C_READY;
	pI2CHandle->pRxBuffer = NULL;
	pI2CHandle->RxLen = 0;
	pI2CHandle->RxSize = 0;

	if(pI2CHandle->I2CConfig.I2C_ACKControl== I2C_ACK_ENABLE)
	{
		I2C_ACKControl(pI2CHandle->pI2Cx,ENABLE);
	}
}

void I2C_SlaveSendData(I2C_regdef_t*pI2Cx,uint8_t data)

{
	pI2Cx->DR = data;
}

uint8_t I2C_SlaveReceiveData(I2C_regdef_t*pI2Cx)
{
    return (uint8_t) pI2Cx->DR;
}
