/*
 * stm32f4xx_usart_driver.c
 *
 *  Created on: Dec 22, 2020
 *       Author: Ashwinder singh garcha
 */


#include "stm32f4xx_usart_driver.h"
#include <stdint.h>
#include <stdio.h>

/***************************************************************************
 * @fn					- USART_PeriClockControl
 *
 * @brief				- Enables or disables clock for a given USART
 *
 * @param[in]			- USART base address
 * @param[in]			- "ENABLE" or "DISABLE" command for the I2Cx clock
 *
 * @return				- none
 *
 * @Note				- none
 */
void USART_PeriClockControl(USART_regdef_t*pUSARTx , uint8_t EnorDI)
{
	if (EnorDI == ENABLE)
	{
		if      (pUSARTx == USART1){USART1_PERI_CLOCK_EN();}
		else if (pUSARTx == USART6){USART6_PERI_CLOCK_EN();}
		else if (pUSARTx == UART4 ){UART4_PERI_CLOCK_EN(); }
		else if (pUSARTx == UART5 ){UART5_PERI_CLOCK_EN(); }
		else if (pUSARTx == USART2){USART2_PERI_CLOCK_EN();}
		else if (pUSARTx == USART3){USART3_PERI_CLOCK_EN();}
	}else
	{
		if      (pUSARTx == USART1){USART1_PERI_CLOCK_DI();}
		else if (pUSARTx == USART6){USART6_PERI_CLOCK_DI();}
		else if (pUSARTx == UART4 ){UART4_PERI_CLOCK_DI(); }
		else if (pUSARTx == UART5 ){UART5_PERI_CLOCK_DI(); }
		else if (pUSARTx == USART2){USART2_PERI_CLOCK_DI();}
		else if (pUSARTx == USART3){USART3_PERI_CLOCK_DI();}

	}

}

void USART_SetBaudRate(USART_regdef_t *pUSARTx, uint32_t BaudRate)
{
	//Variable to hold the APB clock
	uint32_t PCLKx;
	uint32_t usartdiv;
	//variables to hold Mantissa and Fraction values
	uint32_t M_part,F_part;

	uint32_t tempreg=0;
	if(pUSARTx == USART1 ||pUSARTx == USART6 )
	{
		//apb2bus
		PCLKx = RCC_GetPCLK2Value();
	}
	else
	{
		//Apb1bus
		PCLKx = RCC_GetPCLK1Value();
	}

	//Check for OVER8 configuration bit
	if(pUSARTx->CR1 & (1 << USART_CR1_OVER8))
	  {
		   //OVER8 = 1 , over sampling by 8
		   usartdiv = ((25 * PCLKx) / (2 *BaudRate));
	  }else
	  {
		   //over sampling by 16
		   usartdiv = ((25 * PCLKx) / (4 *BaudRate));
	  }

	//Calculate the Mantissa part
	M_part = usartdiv/100;
	//Place the Mantissa part in appropriate bit position . refer USART_BRR
	  tempreg |= M_part << 4;

    //Extract the fraction part
	F_part = (usartdiv - (M_part * 100));

	//Calculate the final fractional
	if(pUSARTx->CR1 & (1 << USART_CR1_OVER8))
	  {
		   //OVER8 = 1 , over sampling by 8
		  F_part = (((F_part *8)+50)/100) & ((uint8_t)0x07);
	  }else
	  {
		   //over sampling by 16
		  F_part = (((F_part *16)+50)/100) & ((uint8_t)0x0F);
	  }
	  //Place the fractional part in appropriate bit position . refer USART_BRR
	  tempreg |= F_part<<0;

	  //copy the value of tempreg in to BRR register
	  pUSARTx->BRR = tempreg;
}
/***************************************************************************
 * @fn					- USART_Init
 *
 * @brief				- Configures USART peripheral register given the
 * 						configuration
 *
 * @param[in]			- USART structure that holds configuration and USART address
 *
 * @return				- none
 *
 * @Note				- none
 */
void USART_Init(USART_Handle_t *pUSARTHandle)
{
	uint32_t tempreg=0;
	/******************************** Configuration of CR1******************************************/
	//1. modes of usart
	if      (pUSARTHandle->USART_Config.USART_Mode == USART_MODE_ONLY_TX)
	{ // tx only
		tempreg|= (1<<USART_CR1_TE );
	}
	else if (pUSARTHandle->USART_Config.USART_Mode == USART_MODE_ONLY_RX)
	{ // rx only
		tempreg|= (1<<USART_CR1_RE );
	}
	else if (pUSARTHandle->USART_Config.USART_Mode == USART_MODE_TXRX)
	{ // both tx and rx
		tempreg|= (1<<USART_CR1_RE )|(1<<USART_CR1_TE );
	}

	//2.WordLength
	tempreg|=  (pUSARTHandle->USART_Config.USART_WordLength <<USART_CR1_M );

	//3.ParityControl
	if      (pUSARTHandle->USART_Config.USART_ParityControl == USART_PARITY_EN_EVEN )
	{ //
		tempreg|=  (1<<USART_CR1_PCE );  //enable parity control
		tempreg&= ~(1<<USART_CR1_PS  );  //enable even parity
	}
	else if (pUSARTHandle->USART_Config.USART_ParityControl == USART_PARITY_EN_ODD )
	{
		tempreg|=  (1<<USART_CR1_PCE );  //enable parity control
		tempreg|=  (1<<USART_CR1_PS  );  //enable odd parity
	}

	////Program the CR1 register
	pUSARTHandle->pUSARTx->CR1 |= tempreg;

	/******************************** Configuration of CR2******************************************/

	tempreg=0;

	//4.stop bits
	tempreg|= (pUSARTHandle->USART_Config.USART_NoOfStopBits << USART_CR2_STOP );

	////Program the CR2 register
	pUSARTHandle->pUSARTx->CR2 |= tempreg;
	/******************************** Configuration of CR3******************************************/

	tempreg=0;

	//5.HWFlowControl
	if(pUSARTHandle->USART_Config.USART_HWFlowControl ==USART_HW_FLOW_CTRL_CTS)
	{
		//enable CTS
	}
	else if (pUSARTHandle->USART_Config.USART_HWFlowControl ==USART_HW_FLOW_CTRL_RTS)
	{
		//enable RTS
	}
	else if (pUSARTHandle->USART_Config.USART_HWFlowControl ==USART_HW_FLOW_CTRL_CTS_RTS)
	{
		//enable RTS and CTS
	}

	////Program the CR3 register
	pUSARTHandle->pUSARTx->CR3 |= tempreg;

	/******************************** Configuration of BRR(Baudrate register)******************************************/

		//Implement the code to configure the baud rate
		//We will cover this in the lecture. No action required here
	USART_SetBaudRate(pUSARTHandle->pUSARTx, pUSARTHandle->USART_Config.USART_Baud);

}
/***************************************************************************
 * @fn					- USART_DeInit
 *
 * @brief				- Configures USART peripheral register given the
 * 						configuration
 *
 * @param[in]			- USART structure that holds configuration and USART address
 *
 * @return				- none
 *
 * @Note				- none
 * */
void USART_DeInit(USART_regdef_t *pUSARTx)
{
	if      (pUSARTx == USART1){USART1_reset();}
	else if (pUSARTx == USART6){USART6_reset();}
	else if (pUSARTx == UART4 ){UART4_reset(); }
	else if (pUSARTx == UART5 ){UART5_reset(); }
	else if (pUSARTx == USART2){USART2_reset();}
	else if (pUSARTx == USART3){USART3_reset();}
}

/*********************************************************************
 * @fn      		  - USART_SendData
 *
 * @brief             -
 *
 * @param[in]         -
 * @param[in]         -
 * @param[in]         -
 *
 * @return            -
 *
 * @Note              - Resolve all the TODOs

 */
void USART_SendData(USART_Handle_t *pUSARTHandle,uint8_t *pTxBuffer, uint32_t Len)
{
	uint16_t *pdata;
	while(Len>0)
	{
		//Implement the code to wait until TXE flag is set in the SR
		while(! USART_GetFlagStatus(pUSARTHandle->pUSARTx,USART_FLAG_TXE));

		//Check the USART_WordLength item for 9BIT or 8BIT in a frame
		if(pUSARTHandle->USART_Config.USART_WordLength == USART_WORDLEN_9BITS)
		{
			//9bit word length
			pdata = (uint16_t*)pTxBuffer;
			pUSARTHandle->pUSARTx->DR = (*pdata & (uint16_t)0x1FF);
			if (pUSARTHandle->USART_Config.USART_ParityControl==USART_PARITY_DISABLE)
			{
				//No parity is used in this transfer. so, 9bits of user data will be sen
				pTxBuffer++;
				pTxBuffer++;
				Len--;
			}
			else
			{
				// parity is used in this transfer. so, 8bits of user data will be sen
				pTxBuffer++;
				Len--;
			}
		}
		else
		{
			//8bit word length
			pUSARTHandle->pUSARTx->DR = (*pTxBuffer & (uint8_t )0xFF);
			pTxBuffer++;
			Len--;
		}
	}

	//Implement the code to wait till TC flag is set in the SR
	while( ! USART_GetFlagStatus(pUSARTHandle->pUSARTx,USART_FLAG_TC ));

}
/*********************************************************************
 * @fn      		  - USART_ReceiveData
 *
 * @brief             -
 *
 * @param[in]         -
 * @param[in]         -
 * @param[in]         -
 *
 * @return            -
 *
 * @Note              -

 */
void USART_ReceiveData(USART_Handle_t *pUSARTHandle, uint8_t *pRxBuffer, uint32_t Len)
{
	while(Len>0)
	{
		//wait until RXNE flag is set in the SR
		while(! USART_GetFlagStatus(pUSARTHandle->pUSARTx,USART_FLAG_RXNE));
		//Check the USART_WordLength to decide whether we are going to receive 9bit of data in a frame or 8 bit
		if (pUSARTHandle->USART_Config.USART_WordLength == USART_WORDLEN_9BITS)
		{
			//We are going to receive 9bit data in a frame
			//Now, check are we using USART_ParityControl control or not
			if(pUSARTHandle->USART_Config.USART_ParityControl == USART_PARITY_DISABLE)
			{
				//No parity is used , so all 9bits will be of user data
				*((uint16_t*)pRxBuffer) = (pUSARTHandle->pUSARTx->DR & (uint16_t)0x01FF);
				pRxBuffer++;
				pRxBuffer++;
				Len--;
			}
			else
			{
				//Parity is used, so 8bits will be of user data and 1 bit is parity
				*pRxBuffer = (pUSARTHandle->pUSARTx->DR & (uint8_t)0x0FF);
				pRxBuffer++;
				Len--;
			}
		}
		else
		{
			//We are going to receive 8bit data in a frame
			//Now, check are we using USART_ParityControl control or not
			if(pUSARTHandle->USART_Config.USART_ParityControl == USART_PARITY_DISABLE)
			{
				//No parity is used , so all 8bits will be of user data
				*pRxBuffer = (uint8_t)(pUSARTHandle->pUSARTx->DR & (uint8_t)0x0FF);

			}
			else
			{
				// parity used 7 bits will be of user data and 1 bit is parity
				*pRxBuffer = (uint8_t)(pUSARTHandle->pUSARTx->DR & (uint8_t)0x07F);
			}
			//Now , increment the pRxBuffer
			pRxBuffer++;
			Len--;
		}
	}
}
/*********************************************************************
 * @fn      		  - USART_SendDataWithIT
 *
 * @brief             -
 *
 * @param[in]         -
 * @param[in]         -
 * @param[in]         -
 *
 * @return            -
 *
 * @Note              - Resolve all the TODOs

 */
uint8_t USART_SendDataIT(USART_Handle_t *pUSARTHandle,uint8_t *pTxBuffer, uint32_t Len);


/*********************************************************************
 * @fn      		  - USART_ReceiveDataIT
 *
 * @brief             -
 *
 * @param[in]         -
 * @param[in]         -
 * @param[in]         -
 *
 * @return            -
 *
 * @Note              - Resolve all the TODOs

 */
uint8_t USART_ReceiveDataIT(USART_Handle_t *pUSARTHandle, uint8_t *pRxBuffer, uint32_t Len);
/*
 * IRQ Configuration and ISR handling
 */
void USART_IRQInterruptConfig(uint8_t IRQNumber, uint8_t EnorDi);
void USART_IRQPriorityConfig(uint8_t IRQNumber, uint32_t IRQPriority);
void USART_IRQHandling(USART_Handle_t *pHandle);

/*********************************************************************
 * @fn      		  - USART_EnableOrDisable
 *
 * @brief             - enable or disable UE bit of CR1 register
 *
 * @param[in]         -
 * @param[in]         -
 * @param[in]         -
 *
 * @return            -
 *
 * @Note              -
 */
void USART_PeripheralControl(USART_regdef_t *pUSARTx, uint8_t EnOrDi)
{
	if (EnOrDi == ENABLE)
	{
		//enable UE
		pUSARTx->CR1 |= (1<<USART_CR1_UE);
	}else
	{
		//disable UE
		pUSARTx->CR1 &= ~(1<<USART_CR1_UE);
	}
}
/*********************************************************************
 * @fn      		  - USART_GetFlagStatus
 *
 * @brief             -
 *
 * @param[in]         -
 * @param[in]         -
 * @param[in]         -
 *
 * @return            -
 *
 * @Note              -
 */
uint8_t USART_GetFlagStatus(USART_regdef_t *pUSARTx , uint32_t FlagName)
{
	if (pUSARTx->SR & FlagName )
	{
		return SET;
	}

	return RESET;
}
void USART_ClearFlag(USART_regdef_t *pUSARTx, uint16_t StatusFlagName);

/*
 * Application callback
 */
void USART_ApplicationEventCallback(USART_Handle_t *pUSARTHandle,uint8_t AppEv);
