/*
 * stm32f4xx_rcc_driver.c
 *
 *  Created on: Dec 23, 2020
 *       Author: Ashwinder singh garcha
 */

#include <stdint.h>
#include"stm32f4xx_rcc_driver.h"

//TO DO: implement PLL clock calculation
/***************************************************************************
 * @fn					- RCC_GetPLLOutputClock
 *
 * @brief				- Helper function to calculate PLL frequency
 *
 * @param[in]			- none
 *
 * @return				- PLL clock
 *
 * @Note				- none
 */
static uint32_t RCC_PLLCLK()
{
	return 0;     // we are not configuring PLL
}
/*********************************************************************
 * @fn      		  - USART_SetBaudRate
 *
 * @brief             -
 *
 * @param[in]         -
 * @param[in]         -
 * @param[in]         -
 *
 * @return            -
 *
 * @Note              -  Resolve all the TODOs

 */
uint32_t RCC_GetPCLK2Value()
{
	uint32_t pclk2,systclk;
	uint8_t clksrce ,temp1,AHB,temp2,APB2;
	// check CFGR for HSI HSE PLL
	clksrce =(((RCC->CFGR) >>2) & 0x3);
	if     (clksrce == 0)
	{systclk = 16000000;}        //HS1 rc oscillator
	else if(clksrce == 1)
	{systclk = 8000000;}         //HSE rc oscillator
	else if(clksrce == 2)
	{systclk =  RCC_PLLCLK();}  //pll

	//check CFGR clock div by AHB
	uint16_t AHBp[8] ={2,4,8,16,64,128,256,512};
	temp1 = (((RCC->CFGR) >>4) & 0xF);
	if     (temp1<8)
	{AHB = 1;}
	else
	{AHB = AHBp[temp1-8];}

	//check CFGR clock div by APB2
	uint8_t APBp2[4] ={2,4,8,16};
	temp2 = (((RCC->CFGR) >>13) & 0x7);
	if     (temp1<4)
	{APB2 = 1;}
	else
	{APB2 = APBp2[temp2-4];}

	pclk2 = ((systclk /AHB ) /APB2);

	return pclk2;

}

uint32_t RCC_GetPCLK1Value()
{
	uint32_t pclk1,systclk;
	uint8_t clksrce1,temp1,AHB,temp2,APB1;
	// check CFGR for HSI HSE PLL
	clksrce1 =(((RCC->CFGR) >>2) & 0x3);
	if     (clksrce1 == 0){systclk = 16000000;}        //HS1 rc oscillator
	else if(clksrce1 == 1){systclk = 8000000;}         //HSE rc oscillator
	else if(clksrce1 == 2){systclk =  RCC_PLLCLK();}  //pll

	//check CFGR clock div by AHB
	uint16_t AHBp[8] ={2,4,8,16,64,128,256,512};
	temp1 = (((RCC->CFGR) >>4) & 0xF);
	if     (temp1<8) {AHB = 1;}
	else  {AHB = AHBp[temp1-8];}

	//check CFGR clock div by APB2
	uint8_t APBp2[4] ={2,4,8,16};
	temp2 = (((RCC->CFGR) >>10) & 0x7);
	if     (temp1<4) {APB1 = 1;}
	else  {APB1 = APBp2[temp2-4];}

	pclk1 = (systclk / AHB) /APB1;
	return pclk1;
}

