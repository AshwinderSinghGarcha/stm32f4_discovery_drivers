/*
 * stm32f4xx.h
 *
 *  Created on: Dec 2, 2020
 *      Author: Ashwinder singh garcha
 */

#ifndef INC_STM32F4XX_H_
#define INC_STM32F4XX_H_

#include<stdint.h>
#include<stddef.h>
/*
 * base addresses of PROCESSOR CORE REGISTER Interrupt Set-enable Registers
 */
#define __vo volatile

#define NVIC_ISER0    ((__vo uint32_t*)0xE000E100U)
#define NVIC_ISER1    ((__vo uint32_t*)0xE000E104U)
#define NVIC_ISER2    ((__vo uint32_t*)0xE000E108U)
#define NVIC_ISER3    ((__vo uint32_t*)0xE000E10CU)
#define NVIC_ISER4    ((__vo uint32_t*)0xE000E110U)
#define NVIC_ISER5    ((__vo uint32_t*)0xE000E114U)
#define NVIC_ISER6    ((__vo uint32_t*)0xE000E118U)
#define NVIC_ISER7    ((__vo uint32_t*)0xE000E11CU)

/*
 * base addresses of PROCESSOR CORE REGISTER Interrupt Clear-enable Registers
 */
#define NVIC_ICER0    ((__vo uint32_t*)0XE000E180U)
#define NVIC_ICER1    ((__vo uint32_t*)0XE000E184U)
#define NVIC_ICER2    ((__vo uint32_t*)0XE000E188U)
#define NVIC_ICER3    ((__vo uint32_t*)0XE000E18CU)
#define NVIC_ICER4    ((__vo uint32_t*)0XE000E190U)
#define NVIC_ICER5    ((__vo uint32_t*)0xE000E194U)
#define NVIC_ICER6    ((__vo uint32_t*)0xE000E198U)
#define NVIC_ICER7    ((__vo uint32_t*)0xE000E19CU)

/*
 * base addresses of PROCESSOR CORE REGISTER Interrupt Clear-enable Registers
 */
#define NO_PR_BIT_IMPLEMENTED   4

#define NVIC_IPR0    ((__vo uint32_t*)0xE000E400U)
//#define NVIC_IPR1    ((__vo uint32t*)0XE000E404U)
//#define NVIC_IPR2    ((__vo uint32t*)0XE000E408U)
//#define NVIC_IPR3    ((__vo uint32t*)0XE000E40CU)
//#define NVIC_IPR4    ((__vo uint32t*)0XE000E410U)
//#define NVIC_IPR5    ((__vo uint32t*)0xE000E414U)
//#define NVIC_IPR6    ((__vo uint32t*)0xE000E418U)
//#define NVIC_IPR7    ((__vo uint32t*)0xE000E41CU)
//#define NVIC_ICER0    (__vo)0XE000E180U
//#define NVIC_ISPR0    (__vo)0XE000E200U
//#define NVIC_ICPR0    (__vo)0XE000E280U
//#define NVIC_IABR0    (__vo)0xE000E400U
//#define NVIC_IPR0     (__vo)0xE000E100U
/*
 * base addresses of SRAM and FLASH memories
 */

#define FLASH_BASEADDR    0x08000000U
#define SRAM1_BASEADDR    0x20000000U
#define SRAM2_BASEADDR    0x2001C000U
#define ROM_BASEADDR      0x1FFF0000U       //system memory
#define SRAM              SRAM1_BASEADDR

/*
 * base addresses of AHBX and APBX bus Peripheral
 */

#define APB1_BASEADDR     0x40000000U       // first peripheral TIM2
#define APB2_BASEADDR     0x40010000U
#define AHB1_BASEADDR     0x40020000U
#define AHB2_BASEADDR     0x50000000U

/*
 * base addresses of  Peripheral Which are hanging on AHB1 bus
 */

#define GPIOA_BASEADDR    (AHB1_BASEADDR +  0x0000)
#define GPIOB_BASEADDR    (AHB1_BASEADDR +  0x0400)
#define GPIOC_BASEADDR    (AHB1_BASEADDR +  0x0800)
#define GPIOD_BASEADDR    (AHB1_BASEADDR +  0x0C00)
#define GPIOE_BASEADDR    (AHB1_BASEADDR +  0x1000)
#define GPIOF_BASEADDR    (AHB1_BASEADDR +  0x1400)
#define GPIOG_BASEADDR    (AHB1_BASEADDR +  0x1800)
#define GPIOH_BASEADDR    (AHB1_BASEADDR +  0x1C00)
#define GPIOI_BASEADDR    (AHB1_BASEADDR +  0x2000)

#define RCC_BASEADDR      (AHB1_BASEADDR +  0x3800)
/*
 * base addresses of  Peripheral Which are hanging on APB1 bus
 */

#define SPI2_BASEADDR    (APB1_BASEADDR +  0x3800)
#define SPI3_BASEADDR    (APB1_BASEADDR +  0x3C00)

#define USART2_BASEADDR  (APB1_BASEADDR +  0x4400)
#define USART3_BASEADDR  (APB1_BASEADDR +  0x4800)
#define UART4_BASEADDR   (APB1_BASEADDR +  0x4C00)
#define UART5_BASEADDR   (APB1_BASEADDR +  0x5000)

#define I2C1_BASEADDR    (APB1_BASEADDR +  0x5400)
#define I2C2_BASEADDR    (APB1_BASEADDR +  0x5800)
#define I2C3_BASEADDR    (APB1_BASEADDR +  0x5C00)

/*
 * base addresses of  Peripheral Which are hanging on APB2 bus
 */

#define USART1_BASEADDR    (APB2_BASEADDR +  0x1000)
#define USART6_BASEADDR    (APB2_BASEADDR +  0x1400)

#define SPI1_BASEADDR      (APB2_BASEADDR +  0x3000)
#define SPI4_BASEADDR      (APB2_BASEADDR +  0x3400)

#define SYSCFG_BASEADDR    (APB2_BASEADDR +  0x3800)
#define EXTI_BASEADDR      (APB2_BASEADDR +  0x3C00)

/***********USART register configuration definition structure***************/
#define __vo volatile
typedef struct
{
	__vo uint32_t SR                ; //0x00 Status register
	__vo uint32_t DR                ; //0x04 Data register
	__vo uint32_t BRR               ; //0x08 Baud rate register
	__vo uint32_t CR1               ; //0x0C Control register 1
	__vo uint32_t CR2               ; //0x10 Control register 2
	__vo uint32_t CR3               ; //0x14 Control register 3
	__vo uint32_t GTPR              ; //0x18 Guard time and prescaler register

}USART_regdef_t;

//USART_regdef_t *pUSARTx = (USART_regdef_t *)USART1_BASEADDR ;
//USART_regdef_t *pUSARTx = USART1 ;
//----------------------apb2 bus
#define USART1                ((USART_regdef_t *)USART1_BASEADDR)
#define USART6                ((USART_regdef_t *)USART6_BASEADDR)
//----------------------apb1 bus
#define USART2                ((USART_regdef_t *)USART2_BASEADDR)
#define USART3                ((USART_regdef_t *)USART3_BASEADDR)
#define UART4                 ((USART_regdef_t *)UART4_BASEADDR )
#define UART5                 ((USART_regdef_t *)UART5_BASEADDR )



/***********I2C register configuration definition structure***************/
#define __vo volatile


typedef struct
{
	__vo uint32_t CR1             ;  //0x00      //comtrol reg 1
	__vo uint32_t CR2             ;  //0x04      //comtrol reg 2
	__vo uint32_t OAR1            ;  //0x08      //Own address register 1
	__vo uint32_t OAR2            ;  //0x0c      //Own address register 2
	__vo uint32_t DR              ;  //0x10      //Data register
	__vo uint32_t SR1             ;  //0x14      //Status register 1
	__vo uint32_t SR2             ;  //0x18      //Status register 2
	__vo uint32_t CCR             ;  //0x1c      //Clock control register
	__vo uint32_t TRISE           ;  //0x20      //TRISE register
	__vo uint32_t FLTR            ;  //0x24      //FLTR register


}I2C_regdef_t;

/*
 * macro for making coding easy
 *
 * SPI_regdef_t *pI2Cx = (I2C_regdef_t *) I2C1_BASEADDR;
 * SPI_regdef_t *pI2Cx = I2C1;
 */
#define I2C1                ((I2C_regdef_t *)I2C1_BASEADDR)
#define I2C2                ((I2C_regdef_t *)I2C2_BASEADDR)
#define I2C3                ((I2C_regdef_t *)I2C3_BASEADDR)
#define I2C4                ((I2C_regdef_t *)I2C4_BASEADDR)

/***********SPI configuration register definition structure***************/
#define __vo volatile


typedef struct
{
	__vo uint32_t CR1              ;  //0x00      //comtrol reg 1
	__vo uint32_t CR2              ;  //0x04      //comtrol reg 2
	__vo uint32_t SR               ;  //0x08      //status register
	__vo uint32_t DR               ;  //0x0c      //data register
	__vo uint32_t CRCPR            ;  //0x10      //crc polynomial reg
	__vo uint32_t RXCRCR           ;  //0x14      //rx crc reg
	__vo uint32_t TXCRCR           ;  //0x18      //tx crc reg
	__vo uint32_t I2SCFGR          ;  //0x1c      //config reg
	__vo uint32_t I2SPR            ;  //0x20      //presclaer


}SPI_regdef_t;

/*
 * macro for making coding easy
 *
 * SPI_regdef_t *pSPIx = (SPI_regdef_t *) SPI1_BASEADDR;
 * SPI_regdef_t *pSPIx = SPI1;
 */
#define SPI1                ((SPI_regdef_t *)SPI1_BASEADDR)
#define SPI2                ((SPI_regdef_t *)SPI2_BASEADDR)
#define SPI3                ((SPI_regdef_t *)SPI3_BASEADDR)
#define SPI4                ((SPI_regdef_t *)SPI4_BASEADDR)


/***********SYSTEM CONFIGURE REGISTER DEFINITION STRUCTURE*****************/
#define __vo volatile
typedef struct
{
	__vo uint32_t MEMRP  ;//: 0x00;//MEM REMAP REGISTER
	__vo uint32_t PMC    ;//: 0x04;//PERIPHERAL MODE CONFIGURATION REG

	__vo uint32_t EXTICR[4];//: 0x08;//exterNAL INTER CONFIGURATION REGISTER 1
	//     uint16_t RES_1  ;//:    16 BIT RESERVED
	//__vo uint16_t EXTICR2;//: 0x0C;//exterNAL INTER CONFIGURATION REGISTER 2
    //     uint16_t RES_2  ;//:    16 BIT RESERVED
    //__vo uint16_t EXTICR3;//: 0x10;//exterNAL INTER CONFIGURATION REGISTER 3
   //      uint16_t RES_3  ;//:    16 BIT RESERVED
   // __vo uint16_t EXTICR4;//: 0x14;//exterNAL INTER CONFIGURATION REGISTER 4
    //     uint16_t RES_4  ;//:    16 BIT RESERVED

	__vo uint32_t CMPCR  ;//: 0x20;//CoMPENSATION CELL CONTROL REG

}SYSCFG_regdef_t;

/* macro for making coding easy
  SYSCFG_regdef_t *pSYSCFG = (SYSCFG_regdef_t *)SYSCFG_BASEADDR ;
  SYSCFG_regdef_t *pSYSCFG = SYSCFG;
 */
#define SYSCFG ((SYSCFG_regdef_t *)SYSCFG_BASEADDR)


/***********external interupt controller CONFIGURE REGISTER DEFINITION STRUCTURE*****************/
#define __vo volatile
typedef struct
{
	__vo uint16_t IMR    ;//: 0x00;//Interrupt mask register
	     uint16_t RES_1  ;//:    16 BIT RESERVED
	__vo uint16_t EMR    ;//: 0x04;//Event mask register
         uint16_t RES_2  ;//:    16 BIT RESERVED
    __vo uint16_t RTSR   ;//: 0x08;//Rising trigger selection register
         uint16_t RES_3  ;//:    16 BIT RESERVED
    __vo uint16_t FTSR   ;//: 0x0C;//Falling trigger selection register
         uint16_t RES_4  ;//:    16 BIT RESERVED
    __vo uint16_t SWIER  ;//: 0x10;//Software interrupt event register
         uint16_t RES_5  ;//:    16 BIT RESERVED
    __vo uint16_t PR     ;//: 0x14;//Pending register
         uint16_t RES_6  ;//:    16 BIT RESERVED


}EXTI_regdef_t;

/* macro for making coding easy
  EXTI_regdef_t *pEXTI = ((EXTI_regdef_t *)EXTI_BASEADDR) ;
  EXTI_regdef_t *pEXTI = EXTI;
 */
#define EXTI ((EXTI_regdef_t *)EXTI_BASEADDR)

/***********PERIPHERAL REGISTER DEFINITION STRUCTURE GPIO*****************/
#define __vo volatile
typedef struct
{
	__vo uint32_t MODER  ;//: 0x00;//GPIO port mode register
	__vo uint16_t OTYPER ;//: 0x04;//GPIO port output type register
	     uint16_t RES_1  ;//:    16 BIT RESERVED
	__vo uint32_t OSPEEDR;//: 0x08;//GPIO port output speed register
	__vo uint32_t PUPDR  ;//: 0x0C;//GPIO port pull-up/pull-down register
	__vo uint16_t IDR    ;//: 0x10;//GPIO port input data register
	     uint16_t RES_2  ;//:    16 BIT RESERVED
	__vo uint16_t ODR    ;//: 0x14;//GPIO port output data register
	     uint16_t RES_3  ;//:    16 BIT RESERVED
	__vo uint32_t BSRR   ;//: 0x18;//GPIO port bit set/reset register
	__vo uint16_t LCKR   ;//: 0x1C;//GPIO port configuration lock register
	     uint16_t RES_4  ;//:    16 BIT RESERVED
	__vo uint32_t AFRL   ;//: 0x20;//GPIO alternate function low register
	__vo uint32_t AFRH   ;//: 0x24;//GPIO alternate function high register
}GPIO_regdef_t;


/* macro for making coding easy
   GPIO_regdef_t *pGPIOA = (GPIO_regdef_t *)GPIOA_BASEADDR;
    GPIO_regdef_t *pGPIOA = GPIOA;
 */

#define GPIOA  ((GPIO_regdef_t *)GPIOA_BASEADDR)
#define GPIOB  ((GPIO_regdef_t *)GPIOB_BASEADDR)
#define GPIOC  ((GPIO_regdef_t *)GPIOC_BASEADDR)
#define GPIOD  ((GPIO_regdef_t *)GPIOD_BASEADDR)
#define GPIOE  ((GPIO_regdef_t *)GPIOE_BASEADDR)
#define GPIOF  ((GPIO_regdef_t *)GPIOF_BASEADDR)
#define GPIOG  ((GPIO_regdef_t *)GPIOG_BASEADDR)
#define GPIOH  ((GPIO_regdef_t *)GPIOH_BASEADDR)
#define GPIOI  ((GPIO_regdef_t *)GPIOI_BASEADDR)

/***********PERIPHERAL REGISTER DEFINITION STRUCTURE RCC*****************/
#define __vo volatile
typedef struct
{
	__vo uint32_t CR      ;//: 0x00;//
	__vo uint32_t PLLCFGR ;//: 0x04;//
	__vo uint32_t CFGR    ;//: 0x08;//
	__vo uint32_t CIR     ;//: 0x0C;//
	__vo uint32_t AHB1RSTR;//: 0x10;//
	__vo uint32_t AHB2RSTR;//: 0x14;//
	__vo uint32_t AHB3RSTR;//: 0x18;//
	     uint32_t RES_1   ;//: 0x1C;//
	__vo uint32_t APB1RSTR;//: 0x20;//
	__vo uint32_t APB2RSTR;//: 0x24;//

	     uint32_t RES_2   ;//: 0x28;//
	     uint32_t RES_3   ;//: 0x2C;//
	__vo uint32_t AHB1ENR ;//: 0x30;//
	__vo uint32_t AHB2ENR ;//: 0x34;//
	__vo uint32_t AHB3ENR ;//: 0x38;//
	     uint32_t RES_4   ;//: 0x3C;//
	__vo uint32_t APB1ENR ;//: 0x40;//
	__vo uint32_t APB2ENR ;//: 0x44;//
	     uint32_t RES_5   ;//: 0x48;//
	     uint32_t RES_6   ;//: 0x4C;//

	__vo uint32_t AHB1LPENR;//: 0x50;//
	__vo uint32_t AHB2LPENR;//: 0x54;//
	__vo uint32_t AHB3LPENR;//: 0x58;//
	     uint32_t RES_7    ;//: 0x5C;//
	__vo uint32_t APB1LPENR;//: 0x60;//
	__vo uint32_t APB2LPENR;//: 0x64;//
         uint32_t RES_8    ;//: 0x68;//
         uint32_t RES_9    ;//: 0x6C;//
	__vo uint32_t BDCR     ;//: 0x70;//
	__vo uint32_t CSR      ;//: 0x74;//
         uint32_t RES_10    ;//: 0x78;//
         uint32_t RES_11   ;//: 0x7C;//
	__vo uint32_t SSCGR    ;//: 0x80;//
    __vo uint32_t PLLI2SCFGR;//: 0x84;//
}RCC_regdef_t;

/* macro for making coding easy
   RCC_regdef_t *pRCC = (RCC_regdef_t *)RCC_BASEADDR;
    RCC_regdef_t *pRCC = RCC;
 */

#define RCC  ((RCC_regdef_t *)RCC_BASEADDR)

//------------------CLOCK ENABLE MACROS---------------------------//
#define SYSCFG_CLOCK_EN()      (RCC->APB2ENR |=(1<<14))
/*
 *cLOCK ENABLE MACRO FOR GPIOX PERIPHERAL
 */
#define GPIOA_PERI_CLOCK_EN()   (RCC->AHB1ENR |=(1<<0))
#define GPIOB_PERI_CLOCK_EN()   (RCC->AHB1ENR |=(1<<1))
#define GPIOC_PERI_CLOCK_EN()   (RCC->AHB1ENR |=(1<<2))
#define GPIOD_PERI_CLOCK_EN()   (RCC->AHB1ENR |=(1<<3))
#define GPIOE_PERI_CLOCK_EN()   (RCC->AHB1ENR |=(1<<4))
#define GPIOF_PERI_CLOCK_EN()   (RCC->AHB1ENR |=(1<<5))
#define GPIOG_PERI_CLOCK_EN()   (RCC->AHB1ENR |=(1<<6))
#define GPIOH_PERI_CLOCK_EN()   (RCC->AHB1ENR |=(1<<7))
#define GPIOI_PERI_CLOCK_EN()   (RCC->AHB1ENR |=(1<<8))


/*
 *cLOCK ENABLE MACRO FOR I2CX PERIPHERAL
 */
#define I2C3_PERI_CLOCK_EN()     (RCC->APB1ENR |=(1<<23))
#define I2C2_PERI_CLOCK_EN()     (RCC->APB1ENR |=(1<<22))
#define I2C1_PERI_CLOCK_EN()     (RCC->APB1ENR |=(1<<21))


/*
 * cLOCK ENABLE MACRO FOR SPIX PERIPHERAL
 */
#define SPI1_PERI_CLOCK_EN()     (RCC->APB2ENR |=(1<<12))
#define SPI4_PERI_CLOCK_EN()     (RCC->APB2ENR |=(1<<13))
#define SPI3_PERI_CLOCK_EN()     (RCC->APB1ENR |=(1<<15))
#define SPI2_PERI_CLOCK_EN()     (RCC->APB1ENR |=(1<<14))

#define SYSCFG_PERI_CLOCK_EN()   (RCC->APB2ENR |=(1<<14))

/*
 * cLOCK ENABLE MACRO FOR USARTX PERIPHERAL
 */

#define USART1_PERI_CLOCK_EN()   (RCC->APB2ENR |=(1<<4))
#define USART6_PERI_CLOCK_EN()   (RCC->APB2ENR |=(1<<5))
#define UART5_PERI_CLOCK_EN()    (RCC->APB1ENR |=(1<<20))
#define UART4_PERI_CLOCK_EN()    (RCC->APB1ENR |=(1<<19))
#define USART3_PERI_CLOCK_EN()   (RCC->APB1ENR |=(1<<18))
#define USART2_PERI_CLOCK_EN()   (RCC->APB1ENR |=(1<<17))



//------------------CLOCK DISBLE MACROS---------------------------//

#define SYSCFG_CLOCK_DI()   (RCC->APB2ENR &= ~(1<<14))
/*
 *cLOCK DISABLE MACRO FOR GPIOX PERIPHERAL
 */
#define GPIOA_PERI_CLOCK_DI()   (RCC->AHB1ENR &= ~(1<<0))
#define GPIOB_PERI_CLOCK_DI()   (RCC->AHB1ENR &= ~(1<<1))
#define GPIOC_PERI_CLOCK_DI()   (RCC->AHB1ENR &= ~(1<<2))
#define GPIOD_PERI_CLOCK_DI()   (RCC->AHB1ENR &= ~(1<<3))
#define GPIOE_PERI_CLOCK_DI()   (RCC->AHB1ENR &= ~(1<<4))
#define GPIOF_PERI_CLOCK_DI()   (RCC->AHB1ENR &= ~(1<<5))
#define GPIOG_PERI_CLOCK_DI()   (RCC->AHB1ENR &= ~(1<<6))
#define GPIOH_PERI_CLOCK_DI()   (RCC->AHB1ENR &= ~(1<<7))
#define GPIOI_PERI_CLOCK_DI()   (RCC->AHB1ENR &= ~(1<<8))
/*
 *cLOCK DISABLE MACRO FOR I2CX PERIPHERAL
 */
#define I2C3_PERI_CLOCK_DI()     (RCC->APB1ENR &= ~(1<<23))
#define I2C2_PERI_CLOCK_DI()     (RCC->APB1ENR &= ~(1<<22))
#define I2C1_PERI_CLOCK_DI()     (RCC->APB1ENR &= ~(1<<21))


/*
 * cLOCK DISABLE MACRO FOR SPIX PERIPHERAL
 */
#define SPI1_PERI_CLOCK_DI()     (RCC->APB2ENR &= ~(1<<12))
#define SPI4_PERI_CLOCK_DI()     (RCC->APB2ENR &= ~(1<<13))
#define SPI3_PERI_CLOCK_DI()     (RCC->APB1ENR &= ~(1<<15))
#define SPI2_PERI_CLOCK_DI()     (RCC->APB1ENR &= ~(1<<14))


#define SYSCFG_PERI_CLOCK_DI()   (RCC->APB2ENR &= ~(1<<14))
/*
 * cLOCK DISABLE MACRO FOR USARTX PERIPHERAL
 */

#define USART1_PERI_CLOCK_DI()   (RCC->APB2ENR &= ~(1<<4))
#define USART6_PERI_CLOCK_DI()   (RCC->APB2ENR &= ~(1<<5))
#define UART5_PERI_CLOCK_DI()    (RCC->APB1ENR &= ~(1<<20))
#define UART4_PERI_CLOCK_DI()    (RCC->APB1ENR &= ~(1<<19))
#define USART3_PERI_CLOCK_DI()   (RCC->APB1ENR &= ~(1<<18))
#define USART2_PERI_CLOCK_DI()   (RCC->APB1ENR &= ~(1<<17))



/*
 *  RESET GPIO POPRTS
 *  WE HAVE TO FIRST SET ANS RESET USING DO WHILE LOOP IS CLEAR ALL REGISTER OF GPIOX PORT
 */
#define GPIOA_RESET()             do {(RCC->AHB1RSTR |= (1<<0));  (RCC->AHB1RSTR &= ~(1<<0));} while(0)
#define GPIOB_RESET()             do {(RCC->AHB1RSTR |= (1<<1));  (RCC->AHB1RSTR &= ~(1<<1));} while(0)
#define GPIOC_RESET()             do {(RCC->AHB1RSTR |= (1<<2));  (RCC->AHB1RSTR &= ~(1<<2));} while(0)
#define GPIOD_RESET()             do {(RCC->AHB1RSTR |= (1<<3));  (RCC->AHB1RSTR &= ~(1<<3));} while(0)
#define GPIOE_RESET()             do {(RCC->AHB1RSTR |= (1<<4));  (RCC->AHB1RSTR &= ~(1<<4));} while(0)
#define GPIOF_RESET()             do {(RCC->AHB1RSTR |= (1<<5));  (RCC->AHB1RSTR &= ~(1<<5));} while(0)
#define GPIOG_RESET()             do {(RCC->AHB1RSTR |= (1<<6));  (RCC->AHB1RSTR &= ~(1<<6));} while(0)
#define GPIOH_RESET()             do {(RCC->AHB1RSTR |= (1<<7));  (RCC->AHB1RSTR &= ~(1<<7));} while(0)
#define GPIOI_RESET()             do {(RCC->AHB1RSTR |= (1<<8));  (RCC->AHB1RSTR &= ~(1<<8));} while(0)


/*
 *  RESET SPI POPRTS
 *  WE HAVE TO FIRST SET ANS RESET USING DO WHILE LOOP IS CLEAR ALL REGISTER OF GPIOX PORT
 */

#define SPI1_reset()               do {(RCC->APB2RSTR |= (1<<12));   (RCC->APB2RSTR &= ~(1<<12));} while(0)
#define SPI2_reset()               do {(RCC->APB1RSTR |= (1<<14));   (RCC->APB1RSTR &= ~(1<<14));} while(0)
#define SPI3_reset()               do {(RCC->APB1RSTR |= (1<<15));   (RCC->APB1RSTR &= ~(1<<15));} while(0)
#define SPI4_reset()               do {(RCC->APB2RSTR |= (1<<13));   (RCC->APB2RSTR &= ~(1<<13));} while(0)  // wrong in datasheet


/*
 *  RESET i2c POPRTS
 *  WE HAVE TO FIRST SET ANS RESET USING DO WHILE LOOP IS CLEAR ALL REGISTER OF GPIOX PORT
 */

#define I2C1_reset()               do {(RCC->APB1RSTR |= (1<<21));     (RCC->APB1RSTR &= ~(1<<21));}while(0)
#define I2C2_reset()               do {(RCC->APB1RSTR |= (1<<22));     (RCC->APB1RSTR &= ~(1<<22));}while(0)
#define I2C3_reset()               do {(RCC->APB1RSTR |= (1<<23));     (RCC->APB1RSTR &= ~(1<<23));}while(0)

/*
 *  RESET USART POPRTS
 *  WE HAVE TO FIRST SET ANS RESET USING DO WHILE LOOP IS CLEAR ALL REGISTER OF GPIOX PORT
 */

#define USART1_reset()               do {(RCC->APB1RSTR |= (1<<4)) ;     (RCC->APB1RSTR &= ~(1<<4)) ;}while(0)
#define USART6_reset()               do {(RCC->APB1RSTR |= (1<<5)) ;     (RCC->APB1RSTR &= ~(1<<5)) ;}while(0)
#define USART2_reset()               do {(RCC->APB1RSTR |= (1<<17));     (RCC->APB1RSTR &= ~(1<<17));}while(0)
#define USART3_reset()               do {(RCC->APB1RSTR |= (1<<18));     (RCC->APB1RSTR &= ~(1<<18));}while(0)
#define UART4_reset()                do {(RCC->APB1RSTR |= (1<<19));     (RCC->APB1RSTR &= ~(1<<19));}while(0)
#define UART5_reset()                do {(RCC->APB1RSTR |= (1<<20));     (RCC->APB1RSTR &= ~(1<<20));}while(0)

// IRQ NUMBERS OF STM32F4

#define IRQ_NO_EXTI0        6
#define IRQ_NO_EXTI1        7
#define IRQ_NO_EXTI2        8
#define IRQ_NO_EXTI3        9
#define IRQ_NO_EXTI4        10
#define IRQ_NO_EXTI9_5      23
#define IRQ_NO_EXTI15_10    40

// spi IRQ NUMBERS OF STM32F4

#define IRQ_NO_SPI1         35
#define IRQ_NO_SPI2         36
#define IRQ_NO_SPI3         51

// I2C IRQ NUMBERS OF STM32F4

#define IRQ_NO_I2C1_EV         31
#define IRQ_NO_I2C1_ER         32
#define IRQ_NO_I2C2_EV         33
#define IRQ_NO_I2C2_ER         34
#define IRQ_NO_I2C3_EV         72
#define IRQ_NO_I2C3_ER         73


// SOME GENERIC MACROS
#define ENABLE          1
#define DISABLE         0
#define SET             ENABLE
#define RESET           DISABLE
#define GPIO_PIN_SET    SET
#define GPIO_PIN_RESET  RESET
#define FLAG_SET    SET
#define FLAG_RESET  RESET
//
#define GPIO_BASEADDR_TO_CODE(x)    ( (x== GPIOA)?0 :\
		                              (x== GPIOB)?1 :\
		                              (x== GPIOC)?2 :\
		                              (x== GPIOD)?3 :\
		                              (x== GPIOE)?4 :\
		                              (x== GPIOF)?5 :\
		                              (x== GPIOG)?6 :\
		                              (x== GPIOH)?7 :0 )


/******************************************************************************************************************
 *                                       bit position definition of SPI peripheral
 ******************************************************************************************************************/
/*
 * bit position SPI_CR1
 */

#define SPI_CR1_CPHA       0
#define SPI_CR1_CPOL       1
#define SPI_CR1_MSTR       2
#define SPI_CR1_BR         3
#define SPI_CR1_SPE        6
#define SPI_CR1_LSBFIRST   7
#define SPI_CR1_SSI        8
#define SPI_CR1_SSM        9
#define SPI_CR1_RXONLY     10
#define SPI_CR1_DFF        11
#define SPI_CR1_CRCNEXT    12
#define SPI_CR1_CRCEN      13
#define SPI_CR1_BIDIOE     14
#define SPI_CR1_BIDIMOOE   15

/*
 * bit position SPI_CR2
 */

#define SPI_CR2_RXDMAEN       0
#define SPI_CR2_TXDMAEN       1
#define SPI_CR2_SSOE          2
#define SPI_CR2_FRF           4
#define SPI_CR2_ERRIE         5
#define SPI_CR2_RXNEIE        6
#define SPI_CR2_TXEIE         7

/*
 * bit position SPIsr
 */

#define SPI_SR_RXNE      0
#define SPI_SR_TXE       1
#define SPI_SR_CHSIDE    2
#define SPI_SR_UDR       3
#define SPI_SR_CRCERR    4
#define SPI_SR_MODF      5
#define SPI_SR_OVR       6
#define SPI_SR_BSY       7
#define SPI_SR_FRE       8

/******************************************************************************************************************
 *                                       bit position definition of I2C peripheral
 ******************************************************************************************************************/
/*
 * bit position I2C_CR1 Control register 1
 */

#define I2C_CR1_PE          0
#define I2C_CR1_SMBUS       1
#define I2C_CR1_SMBTYPE     3
#define I2C_CR1_ENARP       4
#define I2C_CR1_ENPEC       5
#define I2C_CR1_ENGC        6
#define I2C_CR1_NOSTRETCH   7
#define I2C_CR1_START       8
#define I2C_CR1_STOP        9
#define I2C_CR1_ACK         10
#define I2C_CR1_POS         11
#define I2C_CR1_PEC         12
#define I2C_CR1_ALERT       13
#define I2C_CR1_SWRST       15
/*
 * bit position I2C_CR2 Control register 2
 */

#define I2C_CR2_FREQ          0
#define I2C_CR2_ITERREN       8
#define I2C_CR2_ITEVTEN       9
#define I2C_CR2_ITBUFEN       10
#define I2C_CR2_DMAEN         11
#define I2C_CR2_LAST          12


/*
 * bit position I2C_OAR1 Own address register 1
 */

#define I2C_OAR1_ADD0        0
#define I2C_OAR1_ADD1        1
#define I2C_OAR1_ADD8        8
#define I2C_OAR1_ADDMODE     15
/*
 * bit position I2C_OAR2 Own address register 1
 */

#define I2C_OAR2_ENDUAL      0
#define I2C_OAR2_ADD2        1
#define I2C_OAR2_ADD8        8

/*
 * bit position Data register (I2C_DR)
 */

#define I2C_DR               0

/*
 * bit position Status register 1 (I2C_SR1)
 */

#define I2C_SR1_SB             0
#define I2C_SR1_ADDR           1
#define I2C_SR1_BTF            2
#define I2C_SR1_ADD10          3
#define I2C_SR1_STOPF          4
#define I2C_SR1_RxNE           6
#define I2C_SR1_TxE            7
#define I2C_SR1_BERR           8
#define I2C_SR1_ARLO           9
#define I2C_SR1_AF             10
#define I2C_SR1_OVR            11
#define I2C_SR1_PECERR         12
#define I2C_SR1_TIMEOUT        14
#define I2C_SR1_SMBALERT       15

/*
 * bit position Status register 2 (I2C_SR2)
 */

#define I2C_SR2_MSL          0
#define I2C_SR2_BUSY         1
#define I2C_SR2_TRA          2
#define I2C_SR2_GENCALL      4
#define I2C_SR2_SMBDEFAULT   5
#define I2C_SR2_SMBHOST      6
#define I2C_SR2_DUALF        7
#define I2C_SR2_PEC          8

/*
 * bit position Clock control register (I2C_CCR)
 */

#define I2C_CCR_CCR          0
#define I2C_CCR_DUTY        14
#define I2C_CCR_FS          15

/*
 * bit position TRISE register (I2C_TRISE)
 */

#define I2C_TRISE_TRISE      0

/*
 * bit position FLTR register (I2C_FLTR)
 */

#define I2C_FLTR_DNF         0
#define I2C_FLTR_ANOFF       4


/******************************************************************************************************************
 *                                       bit position definition of USART peripheral
 ******************************************************************************************************************/
/*
 * bit position Status register (USART_SR)
 */
#define USART_SR_PE         0
#define USART_SR_FE         1
#define USART_SR_NE         2
#define USART_SR_ORE        3
#define USART_SR_IDLE       4
#define USART_SR_RXNE       5
#define USART_SR_TC         6
#define USART_SR_TXE        7
#define USART_SR_LBD        8
#define USART_SR_CTS        9
/*
 * bit position Data register (USART_DR)
 */
#define USART_DR
/*
 * bit position Baud rate register (USART_BRR)
 */
#define USART_BRR_DIV_Fraction      0
#define USART_BRR_DIV_Mantissa      4
/*
 * bit position Control register 1 (USART_CR1)
 */
#define USART_CR1_SBK         0
#define USART_CR1_RWU         1
#define USART_CR1_RE          2
#define USART_CR1_TE          3
#define USART_CR1_IDLEIE      4
#define USART_CR1_RXNEIE      5
#define USART_CR1_TCIE        6
#define USART_CR1_TXEIE       7
#define USART_CR1_PEIE        8
#define USART_CR1_PS          9
#define USART_CR1_PCE         10
#define USART_CR1_WAKE        11
#define USART_CR1_M           12
#define USART_CR1_UE          13
#define USART_CR1_OVER8       15

/*
 * bit position Control register 2 (USART_CR2)
 */
#define USART_CR2_ADD         0
#define USART_CR2_LBDL        5
#define USART_CR2_LBDIE       6
#define USART_CR2_LBCL        8
#define USART_CR2_CPHA        9
#define USART_CR2_CPOL        10
#define USART_CR2_CLKEN       11
#define USART_CR2_STOP        12
#define USART_CR2_LINEN       14

/*
 * bit position Control register 3 (USART_CR3)
 */
#define USART_CR3_EIE        0
#define USART_CR3_IREN       1
#define USART_CR3_IRLP       2
#define USART_CR3_HDSEL      3
#define USART_CR3_NACK       4
#define USART_CR3_SCEN       5
#define USART_CR3_DMAR       6
#define USART_CR3_DMAT       7
#define USART_CR3_RTSE       8
#define USART_CR3_CTSE       9
#define USART_CR3_CTSIE      10
#define USART_CR3_ONEBIT     11
/*
 * bit position Guard time and prescaler register (USART_GTPR)
 */
#define USART_GTPR_PSC       0
#define USART_GTPR_GT        8
/*
 * bit position Status register (USART_SR)
 */


#include "stm32f4xx_gpio_driver.h"
#include "stm32f4xx_spi_driver.h"
#include "stm32f4xx_i2c_driver.h"
#include "stm32f4xx_usart_driver.h"
#include "stm32f4xx_rcc_driver.h"
#endif /* INC_STM32F4XX_H_ */
