/*
 * stm32f4xx_gpio_driver.h
 *
 *  Created on: Dec 3, 2020
 *       Author: Ashwinder singh garcha
 */

#ifndef INC_STM32F4XX_GPIO_DRIVER_H_
#define INC_STM32F4XX_GPIO_DRIVER_H_

#include"stm32f4xx.h"



/*
 * THIS IS A HANDLE STRUCTURE FOR A GPIO PIN
 */
typedef struct
{
	uint8_t GPIO_PinNumber;         /*@GPIO pin NUMBER*/
	uint8_t GPIO_PinMode;           /*@GPIO pin possible modes*/
	uint8_t GPIO_PinSpeed;          /*@GPIO output speed possible modes*/
	uint8_t GPIO_PinPUPDControl;    /*@GPIO pull-up/pull-down possible modes*/
	uint8_t GPIO_PinOPTYPE;          /*@GPIO port output type PP OR OD possible modes*/
	uint8_t GPIO_PinALTFunMode;      /*@@GPIO pin ALTERNATE FUNCTION possible modes*/

}GPIO_Pinconfig_t;





typedef struct
{   //   GPIO_regdef_t *pGPIOA = (GPIO_regdef_t *)GPIOA_BASEADDR;
    //   GPIO_regdef_t *pGPIOA = GPIOA;
	GPIO_regdef_t *pGPIOx;  //BASEADDRESS YOU CAN GET
	GPIO_Pinconfig_t GPIO_PinConfiguration;
	//EXTI_regdef_t *pEXTI;
}GPIO_Handle_t;

/*
 * @GPIO pin NUMBER
 */
#define GPIO_PIN_NO_0             0
#define GPIO_PIN_NO_1             1
#define GPIO_PIN_NO_2             2
#define GPIO_PIN_NO_3             3
#define GPIO_PIN_NO_4             4
#define GPIO_PIN_NO_5             5
#define GPIO_PIN_NO_6             6
#define GPIO_PIN_NO_7             7
#define GPIO_PIN_NO_8             8
#define GPIO_PIN_NO_9             9
#define GPIO_PIN_NO_10            10
#define GPIO_PIN_NO_11            11
#define GPIO_PIN_NO_12            12
#define GPIO_PIN_NO_13            13
#define GPIO_PIN_NO_14            14
#define GPIO_PIN_NO_15            15
/*
 * @GPIO pin possible modes
 */
#define GPIO_MODE_IN               0
#define GPIO_MODE_OUT              1
#define GPIO_MODE_ALT              2
#define GPIO_MODE_ANA              3
#define GPIO_MODE_IT_FT            4              //INTERRUPT ALSO WORKING IN INPUT MODE FALLING EDGE
#define GPIO_MODE_IT_RT            5              //
#define GPIO_MODE_IT_RFT           6              //RISING FALLING
/*
 * GPIO port output type PP OR OD possible modes
 */
#define GPIO_TPYE_PP               0
#define GPIO_TPYE_OD               1
/*
 * GPIO output speed possible modes
 */
#define GPIO_SPEED_LOW              0
#define GPIO_SPEED_MED              1
#define GPIO_SPEED_HI               2
#define GPIO_SPEED_VHI              3
/*
 * GPIO pull-up/pull-down possible modes
 */
#define GPIO_PUPD_NO              0
#define GPIO_PUPD_PU              1
#define GPIO_PUPD_PD              2
#define GPIO_PUPD_RE              3
/*
 * @GPIO pin ALTERNATE FUNCTION possible modes
 */
#define GPIO_AF0             0
#define GPIO_AF1             1
#define GPIO_AF2             2
#define GPIO_AF3             3
#define GPIO_AF4             4
#define GPIO_AF5             5
#define GPIO_AF6             6
#define GPIO_AF7             7
#define GPIO_AF8             8
#define GPIO_AF9             9
#define GPIO_AF10            10
#define GPIO_AF11            11
#define GPIO_AF12            12
#define GPIO_AF13            13
#define GPIO_AF14            14
#define GPIO_AF15            15

/*
 * @GPIO pin IRQ priority possible modes
 */
#define GPIO_PR0             0
#define GPIO_PR1             1
#define GPIO_PR2             2
#define GPIO_PR3             3
#define GPIO_PR4             4
#define GPIO_PR5             5
#define GPIO_PR6             6
#define GPIO_PR7             7
#define GPIO_PR8             8
#define GPIO_PR9             9
#define GPIO_PR10            10
#define GPIO_PR11            11
#define GPIO_PR12            12
#define GPIO_PR13            13
#define GPIO_PR14            14
#define GPIO_PR15            15
/*
*********************************************************************************************************************
 *                                 Api supported by this driver
 *
 *********************************************************************************************************************/
/*
 * Clock setUP
 */
void GPIO_PeriClkControl(GPIO_regdef_t *pGPIOx,uint8_t ENorDi);
/*
*   Init and DE-Init
 */
void GPIO_Init(GPIO_Handle_t*PGPIOHandle);
void GPIO_DeInit(GPIO_regdef_t *pGPIOx);


/*
 * DATA read and write
 */
uint8_t GPIO_ReadFromInputPin(GPIO_regdef_t *pGPIOx,uint8_t PinNumber);
uint16_t GPIO_ReadFromInputPort(GPIO_regdef_t *pGPIOx);
void GPIO_WritetoOutputPin(GPIO_regdef_t *pGPIOx,uint8_t PinNumber,uint8_t Value);
void GPIO_WritetoOutputPort(GPIO_regdef_t *pGPIOx,uint16_t Value);
void GPIO_ToggleOutputPin(GPIO_regdef_t *pGPIOx,uint8_t PinNumber);
/*
 * IRQ CONFIG
 */
void GPIO_IRQITConfig(uint8_t IRQNumber,uint8_t ENorDi);
void GPIO_IRQPriorityConfig(uint8_t IRQNumber,uint32_t IRQPriority);
void GPIO_IRQHandling(uint8_t PinNumber);


#endif /* INC_STM32F4XX_GPIO_DRIVER_H_ */
