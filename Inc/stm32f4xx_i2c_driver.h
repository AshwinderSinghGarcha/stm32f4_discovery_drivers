/*
 * stm32f4xx_i2c_driver.h
 *
 *  Created on: Dec 14, 2020
 *       Author: Ashwinder singh garcha
 */

#ifndef INC_STM32F4XX_I2C_DRIVER_H_
#define INC_STM32F4XX_I2C_DRIVER_H_

#include <stdint.h>
#include "stm32f4xx.h"

/*
 * configure structure for I2Cx peripheral
 */

typedef struct
{


	uint32_t I2C_SclkSpeed     ; //
	uint8_t  I2C_DeviceAddress ; //
	uint8_t  I2C_ACKControl    ; //
	uint16_t I2C_FMDutyCycle   ; //

}I2C_Config_t;


/*
 * handle structure for I2Cx peripheral
 */

typedef struct
{
	I2C_regdef_t*pI2Cx      ;
	I2C_Config_t I2CConfig  ;
	uint8_t     *pTxBuffer  ;   // to store tx buffer address
	uint8_t     *pRxBuffer  ;   // to store Rx buffer address
	uint32_t     TxLen      ;   // to store Tx length
	uint8_t      RxLen      ;   // to store Rx length
	uint8_t      TxRxState  ;   // to store Communication state
	uint8_t      DevAddr    ;   // to store slave/device address
	uint32_t     RxSize     ;   // to store  rx siZE
    uint8_t      Sr         ;   // to store repeted start value
}I2C_Handle_t;


/*
 * @ I2C_SclkSpeed
 */

#define I2C_SCL_SPEED_SM   100000            //CCR reg  100khz  bit ccr
#define I2C_SCL_SPEED_FM4k 400000            //CCR reg  400khz
#define I2C_SCL_SPEED_FM2k 200000            //CCR reg  200khz

/*
 * @ I2C_ACKControl
 */

#define I2C_ACK_ENABLE    1                    // reg cr1 bit ack
#define I2C_ACK_DISABLE   0

/*
 * @ I2C_FMDutyCycle
 */

#define I2C_DUTY_2      0                     // reg CCR bit DUTY bit 14
#define I2C_DUTY_16_9   1
/*
 * @ I2C_SR
 */

#define I2C_DISABLE_SR      RESET
#define I2C_ENABLE_SR         SET
/**
 * I2C status macros
 */
#define I2C_READY		    0
#define I2C_BUSY_IN_TX	    1
#define I2C_BUSY_IN_RX	    2
/*
 * @ I2C_FLAGS
 */

#define I2C_FLAG_SB      (1  << I2C_SR1_SB  )
#define I2C_FLAG_ADDR    (1  << I2C_SR1_ADDR)
#define I2C_FLAG_BTF     (1  << I2C_SR1_BTF )
#define I2C_FLAG_TxE     (1  << I2C_SR1_TxE )
#define I2C_FLAG_RxNE    (1  << I2C_SR1_RxNE)

#define I2C_FLAG_BERR    (1  << I2C_SR1_BERR)
#define I2C_FLAG_ARLO    (1  << I2C_SR1_ARLO)
#define I2C_FLAG_AF      (1  << I2C_SR1_AF  )
#define I2C_FLAG_OVR     (1  << I2C_SR1_OVR )
#define I2C_FLAG_TIMEOUT (1  << I2C_SR1_TIMEOUT  )

/*
 * @ I2C_app_ev_macros
 *
 */
#define I2C_EV_TX_CMPLT         0
#define I2C_EV_STOP_CMPLT       1
#define I2C_EV_RX_CMPLT         2
/*
 * @ I2C_app_ER_macros
 *
 */
#define I2C_ERROR_BERR  3
#define I2C_ERROR_ARLO  4
#define I2C_ERROR_AF    5
#define I2C_ERROR_OVR   6
#define I2C_ERROR_TIMEOUT 7

/***************************************************************************************************************
 *                                    APIs supported by this driver
 *                    For more info about the APIs check the function definitions
 ***************************************************************************************************************/


/*
 *  peripheral Clock setup
 */
void I2C_PeriClockControl(I2C_regdef_t*pI2Cx , uint8_t EnorDI);


/*
 * Init or DE- init
 */
void I2C_Init(I2C_Handle_t *pI2CHandle);  // need all block for setting everything mode speed , all setting
void I2C_DeInit(I2C_regdef_t*pI2Cx );    //only need base address to rest in RCC regisater



/*
 * Data Send and Recieve
 */
// blocking API
void I2C_MasterSendData(I2C_Handle_t *pI2CHandle,uint8_t *pTxBuffer,uint32_t Len , uint8_t SlaveAddr,uint8_t Sr);
void I2C_MasterReceiveData(I2C_Handle_t *pI2CHandle,uint8_t *pRxBuffer,uint32_t Len , uint8_t SlaveAddr,uint8_t Sr);

//NON BLOACKING API
uint8_t I2C_MasterSendDataIT(I2C_Handle_t *pI2CHandle,uint8_t *pTxBuffer,uint32_t Len , uint8_t SlaveAddr,uint8_t Sr);
uint8_t I2C_MasterReceiveDataIT(I2C_Handle_t *pI2CHandle,uint8_t *pRxBuffer,uint32_t Len , uint8_t SlaveAddr,uint8_t Sr);


void I2C_SlaveSendData(I2C_regdef_t*pI2Cx,uint8_t data);
uint8_t I2C_SlaveReceiveData(I2C_regdef_t*pI2Cx);
/*
 * IRQ Configuration and ISR handling
 */
void I2C_IRQITConfig(uint8_t IRQNumber,uint8_t ENorDi);
void I2C_IRQPriorityConfig(uint8_t IRQNumber,uint32_t IRQPriority);

void I2C_EV_IRQHandling(I2C_Handle_t *pI2CHandle);
void I2C_ER_IRQHandling(I2C_Handle_t *pI2CHandle);

/*
 * Other Peripheral Control APIs
 */

void I2C_PeripheralControl(I2C_regdef_t*pI2Cx, uint8_t EnorDI);
//uint8_t BUSY_flag(I2C_regdef_t*pI2Cx);

void I2C_ACKControl(I2C_regdef_t*pI2Cx, uint8_t EnorDI);
uint8_t I2C_GetFlagStatus(I2C_regdef_t*pI2Cx,uint32_t FlagName  );

/*******APPLICATION CALL BACK *******/
void I2C_ApplicationEventCallBack(I2C_Handle_t *pI2CHandle,uint8_t AppEv);
void I2C_GenerateStop(I2C_regdef_t*pI2Cx);
void I2C_CloseSendData(I2C_Handle_t *pI2CHandle);
#endif /* INC_STM32F4XX_I2C_DRIVER_H_ */

