/*
 * stm32f4xx_rcc_driver.h
 *
 *  Created on: Dec 23, 2020
 *       Author: Ashwinder singh garcha
 */

#ifndef INC_STM32F4XX_RCC_DRIVER_H_
#define INC_STM32F4XX_RCC_DRIVER_H_
#include"stm32f4xx.h"

uint32_t RCC_GetPCLK2Value();
uint32_t RCC_GetPCLK1Value();
#endif /* INC_STM32F4XX_RCC_DRIVER_H_ */
