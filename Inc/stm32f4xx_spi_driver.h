/*
 * stm32f4xx_spi_driver.h
 *
 *  Created on: Dec 8, 2020
 *     Author: Ashwinder singh garcha
 */

#ifndef INC_STM32F4XX_SPI_DRIVER_H_
#define INC_STM32F4XX_SPI_DRIVER_H_

#include <stdint.h>
#include "stm32f4xx.h"
/*
 * configure structure for SPIx peripheral
 */


typedef struct
{
	uint8_t SPI_DevideMode;     // master or slave
	uint8_t SPI_BusConfig ;     // half full simplex
	uint8_t SPI_SclkSpeed ;     // Sclk for APB1 , APB2 max is 21 mhz , 42mhz , in our case is internal 16mhz osc then it is 8mhz
	uint8_t SPI_DFF       ;     // data frame format 8 or 16 bit it select interl 16 or 8 bit shift registers only written when SPI is disabled SPE =0
	uint8_t SPI_CPOL      ;     // clk polarity in idle either 0 or 1  rising or fallign edge
	uint8_t SPI_CPHA      ;     // data capture at trailing oe leading or 1 edge or 2nd edge
	uint8_t SPI_SSM       ;     // software slave mode or hardaware slave mode

}SPI_Config_t;


/*
 * handle structure for SPIx peripheral
 */


typedef struct
{
	SPI_regdef_t *pSPIx   ;        //base address of register SPI 1 2 3 4
	SPI_Config_t SPIConfig;
	uint8_t      *pTxBuffer;       //to store TX buffer addrs
	uint8_t      *pRxBuffer;       // to store Rx buffer addrs
	uint32_t      TxLen;           // TX length
	uint32_t      RxLen;           // rx lenght
	uint8_t       TxState;         // tx state
    uint8_t       RxState;         // rx state

}SPI_Handle_t;

/*
 * possible spi application state
 */
#define SPI_READY                         0
#define SPI_BUSY_IN_RX                    1
#define SPI_BUSY_IN_TX                    2

/*
 * possible spi application EVENTS
 */
#define SPI_EVENT_TX_CMPLT                  1
#define SPI_EVENT_RX_CMPLT                  2
#define SPI_EVENT_OVR_ERR                   3


/*
 * @SPI_DevideMode
 */
#define SPI_MASTER                         1
#define SPI_SLAVE                          0

/*
 * @SPI_BusConfig
 */
#define SPI_FULL_Dup                       1       //full duplex
#define SPI_HALF_Dup                       2       //half duplex
//#define SPI_SIMPLEX_TXONLY                 3       //simplex   // tx is full duplex mode we dont care about rx
#define SPI_SIMPLEX_RXONLY                 3       //simplex
/*
 * @SPI_SclkSpeed                   // check baud rate  register CR1 bit BR[2:0]
 */
#define SPI_SPEED_DIV_2                     0       //clock divide is 2
#define SPI_SPEED_DIV_4                     1       //clock divide is 4
#define SPI_SPEED_DIV_8                     2       //clock divide is 8
#define SPI_SPEED_DIV_16                    3       //clock divide is 16
#define SPI_SPEED_DIV_32                    4       //clock divide is 32
#define SPI_SPEED_DIV_64                    5       //clock divide is 64
#define SPI_SPEED_DIV_128                   6       //clock divide is 128
#define SPI_SPEED_DIV_256                   7       //clock divide is 256

/*
 * @SPI_DFF
 */
#define SPI_DFF_8BIT                        0
#define SPI_DFF_16BIT                       1

/*
 * @SPI_CPOL
 */
#define SPI_CPOL_HIGH                        1
#define SPI_CPOL_LOW                         0

/*
 * @SPI_CPHA
 */
#define SPI_CPHA_HIGH                        1
#define SPI_CPHA_LOW                         0
/*
 * @SPI_SSM
 */
#define SPI_SSM_EN                           1
#define SPI_SSM_DI                           0
/*
 * spi related flags
 */

#define SPI_TXE_FLAG       (1<<SPI_SR_TXE )
#define SPI_RXE_FLAG       (1<<SPI_SR_RXNE )
#define SPI_BUSY_FLAG      (1<<SPI_SR_BSY)
/*
 * spi priority handiling
 */
#define SPI_PR0             0
#define SPI_PR1             1
#define SPI_PR2             2
#define SPI_PR3             3
#define SPI_PR4             4
#define SPI_PR5             5
#define SPI_PR6             6
#define SPI_PR7             7
#define SPI_PR8             8
#define SPI_PR9             9
#define SPI_PR10            10
#define SPI_PR11            11
#define SPI_PR12            12
#define SPI_PR13            13
#define SPI_PR14            14
#define SPI_PR15            15
/***************************************************************************************************************
 *                                    APIs supported by this driver
 *                    For more info about the APIs check the function definitions
 ***************************************************************************************************************/

/*
 *  peripheral Clock setup
 */
void SPI_PeriClockControl(SPI_regdef_t *pSPIx , uint8_t EnorDI);


/*
 * Init or DE- init
 */
void SPI_Init(SPI_Handle_t *pSPIHandle);  // need all block for setting everything mode speed , all setting
void SPI_DeInit(SPI_regdef_t *pSPIx );    //only need base address to rest in RCC regisater



/*
 * Data Send and Recieve
 */
// blocking api if len 1000 it wait unitl all transfer polling base
void SPI_SendData(SPI_regdef_t *pSPIx ,uint8_t *pTxBuffer ,uint32_t Len);
void SPI_ReceiveData(SPI_regdef_t *pSPIx ,uint8_t *pRxBuffer ,uint32_t Len);

// nonblocking api
uint8_t SPI_SendDataIT(SPI_Handle_t *pSPIHandle ,uint8_t *pTxBuffer ,uint32_t Len);
uint8_t SPI_ReceiveDataIT(SPI_Handle_t *pSPIHandle ,uint8_t *pRxBuffer ,uint32_t Len);

/*
 * IRQ Configuration and ISR handling
 */
void SPI_IRQITConfig(uint8_t IRQNumber,uint8_t ENorDi);
void SPI_IRQPriorityConfig(uint8_t IRQNumber,uint32_t IRQPriority);
void SPI_IRQHandling(SPI_Handle_t *pHandle);


/*
 * Other Peripheral Control APIs
 */

void SPI_PeripheralControl(SPI_regdef_t *pSPIx, uint8_t EnorDI);

void SPI_SSIConfig(SPI_regdef_t *pSPIx, uint8_t EnorDI);


void SPI_SSOEConfig(SPI_regdef_t *pSPIx, uint8_t EnorDI);

uint8_t BUSY_flag(SPI_regdef_t *pSPIx);

void SPI_ClearOVRflag(SPI_Handle_t *pSPIHandle);
void SPI_CloseTranmission(SPI_Handle_t *pSPIHandle);
void SPI_CloseReception(SPI_Handle_t *pSPIHandle);


/**************/
void SPI_ApplicationEventCallBack(SPI_Handle_t *pSPIHandle,uint8_t AppEv);



#endif /* INC_STM32F4XX_SPI_DRIVER_H_ */
